export const SITE_TITLE = 'Mintlayer';

export const LOCAL_STORAGE_KEYS = {
	settings: 'Mintlayer_v01_settings',
	accessToken: 'Mintlayer_v01_accessToken'
}

export const URL_API = process.env.NEXT_PUBLIC_URL_API;

export const ONBOARDING_FORWARDER = 'ONBOARDING_FORWARDER';

export const LAUNCH_URL = 'https://launch.mintlayer.org/token-generation';

export const LANGUAGES = {
  en: '/static/icons/uk_flag.svg',
  de: '/static/icons/de_flag.svg',
  es: '/static/icons/es_flag.svg'
};

export const ROUTES = {
	index: '/',
	staking: `/staking`,
	yourTokens: `/your-tokens`,
	createVestingSchedules: `/create-vesting-schedules`,
}

export const HIDDEN_LABEL = '******';