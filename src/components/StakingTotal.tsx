import numeral from "numeral";
import NextImage from "next/image";
import { Box, Card, CardContent, Grid, Typography } from "@mui/material";

import { GridContainer } from "./GridContainer";
import { MLT_SYMBOL } from "src/data/vesting_data_dapp";

/* types */
import type { FC } from "react";
import { display, SxProps, Theme } from "@mui/system";

const dividerVertical: SxProps<Theme> = {
  borderRightWidth: 1,
  borderRightStyle: 'solid',
  borderRightColor: 'neutral.400',
};

export const StakingTotal: FC = () => {
  return (
    <Card
      sx={{
        backgroundImage: `
          linear-gradient(
            104.04deg,
            hsla(170, 80%, 33%, 1),
            hsla(170, 80%, 33%, 0.6) .01%,
            hsla(170, 80%, 33%, 0.1)
          ),
          url(/static/assets/staking_bg_v2.jpg)
        `,
        backgroundSize: 'cover',
        backgroundPosition: 'center'
      }}
    >
      <CardContent sx={{ py: 5 }}>
        <Typography variant="h6">
          Total Staked
        </Typography>

        <Typography variant="h5">
          {`${numeral(40_000_000).format('0,000.00')} ${MLT_SYMBOL}`}
        </Typography>

        <Box my={4}/>

        <Typography variant="h6">
          Estimate reward
        </Typography>

        <Typography variant="h5">
          {`${numeral(40).format('0,000.00')}%`}
        </Typography>
      </CardContent>
    </Card>
  )
}
