import React from 'react';
import { styled, Grid } from '@mui/material';

/* types */
import type { GridProps } from '@mui/material';

export const GridContainer = styled((props: GridProps) => {
  return <Grid container spacing={2} px={4} { ...props } />
})``;