import {useState } from 'react';
import { styled } from '@mui/material/styles';

import Footer from './Footer';
import { Navbar } from './Navbar';

/* types */
import type { FC, ReactNode } from 'react';

interface MainLayoutProps {
  children?: ReactNode;
}

const MainLayoutRoot = styled('div')(
  ({ theme }) => ({
    backgroundColor: theme.palette.background.default,
    height: '100%',
    paddingTop: 64
  })
);

const MainLayout: FC<MainLayoutProps> = ({ children }) => {
  return (
    <MainLayoutRoot>
      <Navbar />

      {children}

      <Footer />
    </MainLayoutRoot>
  );
};

export default MainLayout;