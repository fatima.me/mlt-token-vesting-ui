import numeral from 'numeral';
import NextLink from 'next/link';
import { BigNumber } from 'ethers';
import { useState, useEffect } from 'react';
import { parseEther, formatEther } from 'ethers/lib/utils';
import { ExternalLinkIcon } from 'src/icons/ExternalLinkIcon';
import {
  Card,
  Grid,
  Link,
  Skeleton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography
} from '@mui/material';

import VestingAPI from 'src/lib/VestingAPI';
import { useWeb3 } from 'src/hooks/useWeb3';

/* types */
import { AllocationWithTokenCount } from 'src/types/vesting';

export const PoolTotalsTable = () => {
  const { addresses } = useWeb3();
  const [ allocations, setAllocations ] = useState<AllocationWithTokenCount | null>(null);

  const address = addresses[0];

  useEffect(() => {
    async function init() {
      const { status, data } = await VestingAPI.getAllocation(address);

      if(status) {
        setAllocations(data);
      }
    }

    init();
  }, [ address ])

  return (
    <Grid container>
      <Grid item xs={12} mb={4}>
        <Typography variant='h5'>
          Pool totals
        </Typography>
      </Grid>

      <Grid item xs={12}>
        <Card>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>
                  Pool name
                </TableCell>

                <TableCell>
                  Token Amount
                </TableCell>

                <TableCell>
                  Pool Share
                </TableCell>

                <TableCell>
                  Locked Tokens
                </TableCell>

                <TableCell>
                  Claimed Tokens
                </TableCell>

                <TableCell>
                  Claimable Tokens
                </TableCell>

                <TableCell>
                  Vesting Type
                </TableCell>
              </TableRow>
            </TableHead>

            <TableBody>
              {!allocations ? (
                <TableRow>
                  <TableCell colSpan={6}>
                    <Skeleton variant='rectangular' />
                  </TableCell>
                </TableRow>
              ) : (Object.entries(allocations).map(([ key, data ]) => {
                const {
                  label,
                  poolShare,
                  tokenAmount,
                  vestingType,
                  lockedTokens,
                  claimedTokens,
                  claimableTokens,
                } = data;

                return(
                  <TableRow key={key}>
                    <TableCell>
                      {label}
                    </TableCell>

                    <TableCell>
                      {numeral(tokenAmount).format('0,000.00')}
                    </TableCell>

                    <TableCell>
                      {`${numeral(poolShare).format('0.00')}%`}
                    </TableCell>

                    <TableCell>
                      {`${numeral(lockedTokens).format('0,000.00')}`}
                    </TableCell>

                    <TableCell>
                      {`${numeral(claimedTokens).format('0,000.00')}`}
                    </TableCell>

                    <TableCell>
                      {`${numeral(claimableTokens).format('0,000.00')}`}
                    </TableCell>

                    <TableCell>
                      <NextLink
                        passHref
                        href={`https://docs.mintlayer.org/whitepaper/6-token-and-public-sale#62-token-distribution--`}
                      >
                        <Link
                          target='_blank'
                          sx={{display: 'flex', gridGap: 6, alignItems: 'center'}}
                        >
                          {vestingType}
                          <ExternalLinkIcon fontSize='small'/>
                        </Link>
                      </NextLink>
                    </TableCell>
                  </TableRow>
                )
              }))}

              {allocations && (
                <TableRow
                  sx={{
                    '& td': {
                      fontWeight: '700'
                    }
                  }}
                >
                  <TableCell>
                    TOTAL
                  </TableCell>

                  <TableCell>
                    {numeral(
                      Object.values(allocations).reduce((prev, { tokenAmount }) => {
                        return prev + parseFloat(tokenAmount);
                      }, 0)
                    ).format('0,000.00')}
                  </TableCell>

                  <TableCell>
                    {`${numeral(
                      Object.values(allocations).reduce((prev, { poolShare }) => {
                        return prev + poolShare;
                      }, 0)
                    ).format('0.00')}%`}
                  </TableCell>

                  <TableCell>
                    {numeral(
                      formatEther(
                        Object.values(allocations).reduce((prev, { lockedTokens }) => {
                          return prev.add(parseEther(`${lockedTokens}`));
                        }, BigNumber.from(0))
                      )
                    ).format('0,000.00')}
                  </TableCell>

                  <TableCell>
                    {numeral(
                      formatEther(
                        Object.values(allocations).reduce((prev, { claimedTokens }) => {
                          return prev.add(parseEther(`${claimedTokens}`));
                        }, BigNumber.from(0))
                      )
                    ).format('0,000.00')}
                  </TableCell>

                  <TableCell>
                    {numeral(
                      formatEther(
                        Object.values(allocations).reduce((prev, { claimableTokens }) => {
                          return prev.add(parseEther(`${claimableTokens}`))
                        }, BigNumber.from(0))
                      )
                    ).format('0,000.00')}
                  </TableCell>

                  <TableCell/>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </Card>
      </Grid>
    </Grid>
  )
}