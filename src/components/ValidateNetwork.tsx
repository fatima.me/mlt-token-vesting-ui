import { BigNumber } from 'ethers';
import toast from 'react-hot-toast';
import { io } from 'socket.io-client';
import { useEffect, useState } from 'react';
import { hexValue } from 'ethers/lib/utils';
import { useTranslation } from 'react-i18next';
import { Box, Button, Grid, Typography } from '@mui/material';

import { URL_API } from 'src/constants';
import VestingAPI from 'src/lib/VestingAPI';
import { useWeb3 } from 'src/hooks/useWeb3';
import { SplashScreen } from './SplashScreen';
import { GridContainer } from './GridContainer';
import { toastError } from 'src/utils/toastError';
import { globalStyles as styles } from 'src/styles/global';
import {
  NETWORK_NAME,
  NETWORK_URL_RPC,
  NETWORK_CHAIN_ID,
  NETWORK_DECIMALS,
  NETWORK_EXPLORER_URL,
  NETWORK_CURRENCY_NAME,
  NETWORK_CURRENCY_SYMBOL,
} from 'src/data/vesting_data_dapp';

/* types */
import type { FC, ReactNode } from 'react';
import type { TreeLog } from 'src/types/vesting';

interface ValidateNetworkProps {
  children: ReactNode;
}

const socket = io(URL_API);

export const ValidateNetwork: FC<ValidateNetworkProps> = (props) => {
  const { children } = props;
  const { t } = useTranslation();
  const { addresses } = useWeb3();
  const [ checked, setChecked ] = useState(false);
  const [ chainIdCurrent, setChainIdCurrent ] = useState(BigNumber.from('0'));

  const connectedAddress = addresses[0];

  const messageInfo = t(
    'To continue you must change to the "{{chain}}" chain', {chain: NETWORK_NAME}
  );

  const NETWORK_CHAIN_ID_DEFAULT = BigNumber.from(NETWORK_CHAIN_ID);

  useEffect(() => {
    if(checked && NETWORK_CHAIN_ID_DEFAULT.eq(chainIdCurrent)) {
      async function processLogs(log: TreeLog) {
        try {
          const { status, treeLogId, treeLogTx, userAddress } = log;

          if(connectedAddress.toLowerCase() == userAddress.toLowerCase()) {
            const duration = 1000 * 60 * 60;

            if(!status) {
              toastError(
                <Box>
                  <Typography variant='body1'>
                    An error occurred while trying to add a new TGE. You can see the cause of
                    the error by clicking
                    {' '}
                    <Box
                      component='a'
                      target='_blank'
                      href={`${NETWORK_EXPLORER_URL}/tx/${treeLogTx}`}
                    >
                      here
                    </Box>
                  </Typography>
                </Box>,
                duration
              )
            }

            if(status) {
              toast.success((t) => {
                return (
                  <div onClick={() => toast.dismiss(t.id)}>
                    <Box>
                      <Typography variant='body1'>
                        New TGE added successfully
                      </Typography>
                    </Box>
                  </div>
                )
              }, { duration })
            }

            VestingAPI.deleteTreeLog(treeLogId, userAddress);
          }

        } catch(error) {
          console.log('> Error in processLogs', error);
        }
      }

      socket.on('treeLogs', processLogs);

      return () => {
        socket.off('treeLogs', processLogs);
      }
    }
  }, [ checked, chainIdCurrent ])

  useEffect(() => {
    if(window.ethereum) {
      function handleChainChanged(chainId: string) {
        try {
          const chainIdCurrentBN = BigNumber.from(chainId);
          if(!NETWORK_CHAIN_ID_DEFAULT.eq(chainIdCurrentBN)) {
            setChainIdCurrent(chainIdCurrentBN);
          }
        } catch(error) {
          console.log('handleChainChanged error', error);
        }
      }

      async function init() {
        try {
          const _chainIdCurrent = await window.ethereum.request({ method: 'eth_chainId' });

          setChainIdCurrent(BigNumber.from(_chainIdCurrent));
          setChecked(true);

        } catch(error) {
          console.log(error);
        }
      }

      init();

      window.ethereum.on('chainChanged', handleChainChanged);

      return () => {
        window.ethereum.removeListener('chainChanged', handleChainChanged);
      }
    }
  }, []);

  const changeChain = async () => {
    try {
      await window.ethereum.request({
        method: 'wallet_switchEthereumChain',
        params: [{ chainId: hexValue(NETWORK_CHAIN_ID) }]
      })

      setChainIdCurrent(NETWORK_CHAIN_ID_DEFAULT)

    } catch(error) {
      console.log('> error in changeChain', error);

      if(error.code == 4902) {
        try {
          toast.error(
            t('The network does not exist. Please add the desired network, then switch to it.')
          );

          await window.ethereum.request({
            method: 'wallet_addEthereumChain',
            params: [{
              chainId: hexValue(NETWORK_CHAIN_ID),
              rpcUrls: [ NETWORK_URL_RPC ],
              chainName: NETWORK_NAME,
              nativeCurrency: {
                decimals: parseInt(NETWORK_DECIMALS),
                name: NETWORK_CURRENCY_NAME,
                symbol: NETWORK_CURRENCY_SYMBOL,
              },
              blockExplorerUrls: [ NETWORK_EXPLORER_URL ],
            }]
          })

          setChainIdCurrent(NETWORK_CHAIN_ID_DEFAULT)
        } catch(error) {
          console.log(error);

          if(error.message) {
            toast.error(error.message);
          }
        }
      } else {
        if(error.message) {
          toast.error(error.message);
        }
      }
    }
  }

  if(!checked) {
    return <SplashScreen/>
  }

  if(checked && !NETWORK_CHAIN_ID_DEFAULT.eq(chainIdCurrent)) {
    return(
      <Box component="main" sx={styles.containerMain} >
        <GridContainer mb={10} textAlign='center'>
          <Grid item xs={12}>
            <Typography variant='h4'>
              {messageInfo}
            </Typography>

            <Box mt={5}>
              <Button variant='contained' onClick={changeChain}>
                Change chain
              </Button>
            </Box>
          </Grid>
        </GridContainer>
      </Box>
    )
  }

  // If got here, it means that the redirect did not occur, and that tells us that the user is
  // authenticated / authorized.
  return <>{children}</>;
};