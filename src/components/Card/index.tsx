import { Box } from "@mui/material";

/* types */
import { styles } from "./styles";

interface CardProps {
  children: React.ReactNode;
}

const Card = ({children}: CardProps) => {
  return (
    <Box sx={styles.card}>
      {children}
    </Box>
  )
}

export default Card;
