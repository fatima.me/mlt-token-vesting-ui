import type { SxProps, Theme } from "@mui/system";

type S = SxProps<Theme>;

const boxLogo: S = {
  width: 60,
  padding: 1,
  borderRadius: 2,
  background: '#FFFFFF',
}

const cardWrapper: S = {
  backgroundColor: 'background.contrast'
}

const cardActionArea: S = {
  padding: 2,
}

const toastInstallMetamaskButtons: S = {
  mt:1,
  gridGap: 10,
  display: 'grid',
  gridTemplateColumns: 'repeat(2, max-content)',
}

const cardContent: S = {
  gridGap: 16,
  display: 'grid',
  gridTemplateColumns: '60px 1fr 60px',
}

const label: S = {
  display: 'flex',
  alignItems: 'center'
}

export const styles = {
  boxLogo,
  cardWrapper,
  cardContent,
  label,
  cardActionArea,
  toastInstallMetamaskButtons
}