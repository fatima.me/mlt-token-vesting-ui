import toast from 'react-hot-toast';
import { useTranslation } from 'react-i18next';
import React, { useEffect, useRef } from 'react';
import MetaMaskOnboarding from '@metamask/onboarding';
import { Box, Card, Typography, CardActionArea, Button } from '@mui/material';

import { styles } from './styles';
import { useWeb3 } from 'src/hooks/useWeb3';
import { MetamaskLogo } from './MetamaskLogo';
import { ONBOARDING_FORWARDER } from 'src/constants';

export const Metamask = () => {
  const { t } = useTranslation();
  const { connectWallet } = useWeb3();
  const onboarding = useRef<MetaMaskOnboarding>();

  useEffect(() => {
    function receiveMessage(event: MessageEvent) {
      if(onboarding.current) {
        // @ts-ignore
        const origin = onboarding?.current?.forwarderOrigin;

        if(event.origin === origin) {
          if(event.data.type === 'metamask:reload') {
            // Save a flag to identify the user to install metamas via onboarding
            sessionStorage.setItem(ONBOARDING_FORWARDER, '1');
          }
        }
      }
    }

    if(!onboarding.current) {
      onboarding.current = new MetaMaskOnboarding();

      window.addEventListener('message', receiveMessage);
    }

    return () => {
      window.removeEventListener('message', receiveMessage);
    }
  }, [])


  const isTrustWalletInstalled = () => {
    return Boolean(
      (window as any).ethereum && (window as any).ethereum.isTrust,
    );
  }

  const handleConnectWallet = async () => {
    if(MetaMaskOnboarding.isMetaMaskInstalled() || isTrustWalletInstalled()) {
      connectWallet();

      if(onboarding.current) {
        onboarding.current.stopOnboarding();
      }
    } else {
      if(onboarding.current) {
        toast((toastRef) => {
          const handleInstall = () => {
            handleCancel();
            onboarding.current.startOnboarding();
          }

          const handleCancel = () => {
            toast.dismiss(toastRef.id)
          }

          return(
            <Box>
              <Typography>
                You must install Metamask or Trust wallet, both virtual Ethereum wallets, in your browser.
              </Typography>

              <Box sx={styles.toastInstallMetamaskButtons}>
                <Button variant='contained' size='small' onClick={handleInstall}>
                  Install
                </Button>

                <Button variant='outlined' size='small' onClick={handleCancel}>
                  Cancel
                </Button>
              </Box>
            </Box>
          )
        }, { id: 'installWallet', duration: 20000, icon: '' })
      }
    }
  }

  return (
    <Card sx={styles.cardWrapper}>
      <CardActionArea sx={styles.cardActionArea} onClick={handleConnectWallet}>
        <Box sx={styles.cardContent}>
          <Box sx={styles.boxLogo}>
            <MetamaskLogo/>
          </Box>

          <Box sx={styles.label}>
            <Typography>
              Connect to MetaMask / Trust Wallet
            </Typography>
          </Box>
        </Box>
      </CardActionArea>
    </Card>
  )
}