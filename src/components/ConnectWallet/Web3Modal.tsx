import React from 'react';
import { useTranslation } from 'react-i18next';
import {
  Box,
  Dialog,
  DialogContent,
  Grid,
  IconButton,
  Typography
} from '@mui/material';

import { Metamask } from './Metamask';

/* icons */
import { X as XIcon } from 'src/icons/x';
import { InfoCircleOutlined as InfoCircleOutlinedIcon } from 'src/icons/InfoCircleOutlined';

/* types */
import type { FC } from 'react';
import type { Web3ModalProps } from 'src/types/connectWallet';

const Web3Modal: FC<Web3ModalProps> = (props) => {
  const { open, onClose } = props;
  const { t } = useTranslation();

  const AVAILABLE_WALLETS = [
    Metamask,
  ];

  return (
    <Dialog
      fullWidth
      open={open}
      maxWidth="sm"
      onClose={onClose}
    >
      <Box
        sx={{
          p: 2,
          display: 'flex',
          alignItems: 'center',
          color: 'primary.contrastText',
          backgroundColor: 'primary.main',
          justifyContent: 'space-between',
        }}
      >
        <Typography variant="h6">
          Available wallets
        </Typography>

        <IconButton color="inherit" onClick={onClose} >
          <XIcon fontSize="small" />
        </IconButton>
      </Box>

      <DialogContent>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Box sx={{ display: 'flex' }}>
              <InfoCircleOutlinedIcon sx={{ mr: 1 }} />

              <Typography>
                Make sure to select all accounts that you want to grant access to.
              </Typography>
            </Box>
          </Grid>

          {AVAILABLE_WALLETS.map((Wallet, key) => {
            return(
              <Grid item key={key}>
                <Wallet />
              </Grid>
            )
          })}
        </Grid>
      </DialogContent>
    </Dialog>
  )
}

export default Web3Modal