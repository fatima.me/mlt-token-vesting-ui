import { useTranslation } from 'react-i18next';
import React, { useEffect, useState } from 'react';
import { Avatar, Button, Skeleton } from '@mui/material';

import Web3Modal from './Web3Modal';
import { useWeb3 } from 'src/hooks/useWeb3';
import { ONBOARDING_FORWARDER } from 'src/constants';
import { shortAddress } from 'src/utils/shortAddress';

/* types */
import type { FC } from 'react';
import { Web3ModalButtonProps } from 'src/types/connectWallet';

const Web3ModalButton: FC<Web3ModalButtonProps> = (props) => {
  const { title } = props;
  const { t } = useTranslation();
  const { addresses } = useWeb3();
  const [ openModal, setOpenModal ] = useState(false);

  useEffect(() => {
    if(addresses.length) {
      setOpenModal(false);
    }
  }, [ addresses ])

  useEffect(() => {
    // Open the modal if the user just installed metamask via onboarding
    const isForwarder = sessionStorage.getItem(ONBOARDING_FORWARDER);

    if(isForwarder) {
      setOpenModal(true);
      sessionStorage.removeItem(ONBOARDING_FORWARDER);
    }
  }, [])

  const togglerModal = () => {
    setOpenModal((prevState) => !prevState);
  }

  const handleCloseModal = () => {
    setOpenModal(false);
  }

  const currentAddress = addresses.length ? addresses[0] : '';

  return (
    <>
      <Button
        variant='contained'
        onClick={togglerModal}
        startIcon={(!openModal && currentAddress) ? (
          <Avatar src='/static/wallets/wallet-icon.png' sx={{ width: 24, height: 24 }} />
        ): null}
      >
        {
          openModal ? (
            <Skeleton
              variant='rectangular'
              width={100}
              height={24}
              sx={{ borderRadius: 1 }}
              animation='wave'
            />
          ) : (currentAddress)
            ? shortAddress(currentAddress)
            : title || 'Connect wallet'
        }
      </Button>

      <Web3Modal
        open={openModal}
        onClose={handleCloseModal}
      />
    </>
  )
}

export default Web3ModalButton