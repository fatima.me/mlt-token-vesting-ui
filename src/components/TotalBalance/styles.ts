import type { SxProps, Theme } from '@mui/material';

type S = SxProps<Theme>;

const totalBalance: S = {
  mb: '3px',
  padding: 2,
  background: `
  linear-gradient(
    104.04deg,
    hsla(170, 80%, 33%, 1),
    hsla(170, 80%, 33%, 0.6) .01%,
    hsla(170, 80%, 33%, 0.1)
  ),
  hsla(170, 28%, 92%, 1)
`
};

export const styles = {
  totalBalance,
};
