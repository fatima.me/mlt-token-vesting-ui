import numeral from "numeral";
import { Skeleton, Typography } from "@mui/material";

import Card from "src/components/Card";
import { HIDDEN_LABEL } from "src/constants";

/* types */

interface TotalBalanceProps {
  hidden: boolean;
  balance?: string;
}

const TotalBalance = ({ balance, hidden }: TotalBalanceProps) => {
  return (
    <Card>
      <Typography variant="caption">
        Your total balance is:
      </Typography>
      
      <Typography variant="h5">
        {!balance ? (
          <Skeleton sx={{ display: 'inline-block', width: 180 }} />
        ): (
          hidden ? HIDDEN_LABEL : `${numeral(balance).format('0,000.00')} ML` 
        )}
      </Typography>
    </Card>
  )
}

export default TotalBalance;
