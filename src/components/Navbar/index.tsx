import NextLink from "next/link";
import { useContext } from "react";
import {
  Box,
  AppBar,
  Toolbar,
  Container,
  IconButton,
} from "@mui/material";

import { Logo } from "../logo";
import NavList from "../NavList";
import SideBar from "../SideBar";
import { styles } from "./styles";
import { ROUTES } from "src/constants";
import { useWeb3 } from "src/hooks/useWeb3";
import SideBarContent from "../SideBarContent";
import { Menu as MenuIcon } from "src/icons/Menu";
import { AppContext } from "src/contexts/app.context";
import Web3ModalButton from "../ConnectWallet/Web3ModalButton";

const NAV_LIST = [
  {
    title: "Your tokens",
    href: ROUTES.yourTokens,
  },
  {
    title: "Stake",
    href: ROUTES.staking,
  },
];

export const Navbar = () => {
  const { isAuthenticated, isInitialized } = useWeb3();
  const { sideBarOpen, setSideBarOpen } = useContext(AppContext);

  const handleSideBarOpen = () => {
    setSideBarOpen(true);
  };

  return (
    <AppBar elevation={0} sx={styles.appBar}>
      <Container maxWidth="lg">
        <Toolbar disableGutters sx={styles.toolBar}>
          <NextLink href="/" passHref>
            <a>
              <Logo sx={styles.logo} />
            </a>
          </NextLink>

          <Box sx={{ display: "flex", flexGrow: 1, justifyContent: "center" }}>
              {isAuthenticated && isInitialized && <NavList list={NAV_LIST}/>}
          </Box>

          <IconButton onClick={handleSideBarOpen} sx={styles.mobileIconMenu}>
            <MenuIcon fontSize="small" />
          </IconButton>

          <Box sx={styles.wrapperLinks}>
            <Web3ModalButton />
          </Box>
        </Toolbar>
      </Container>
      {sideBarOpen &&
        <SideBar>
          <SideBarContent list={NAV_LIST} />
        </SideBar>}
    </AppBar>
  );
};
