import type { SxProps, Theme } from '@mui/material';

type S = SxProps<Theme>;

const appBar: S = {
  backgroundColor: 'background.light',
  borderBottomColor: 'divider',
  borderBottomStyle: 'solid',
  borderBottomWidth: 1,
  color: 'text.secondary'
};

const toolBar = {
  minHeight: {xs: 80, md: 100},
}

const mobileIconMenu: S = {
  display: {
    md: 'none'
  },
  color: 'inherit',
};

const wrapperLinks: S = {
  alignItems: 'center',
  display: {
    md: 'flex',
    xs: 'none'
  }
}

const logo: S = {};

export const styles = {
  appBar,
  logo,
  mobileIconMenu,
  wrapperLinks,
  toolBar
};
