import numeral from 'numeral';
import { formatEther } from 'ethers/lib/utils';
import { useTranslation } from 'react-i18next';
import { useTheme } from '@mui/material/styles';
import { Box, Typography } from '@mui/material';

import { Chart } from 'src/components/Chart';

/* types */
import type { FC } from 'react';
import type { ApexOptions } from 'apexcharts';
import type { TokenDistributionProps } from 'src/types/vesting';

export const TokenDistribution: FC<TokenDistributionProps> = (props) => {
  const {
    lockedTokens,
    totalVesting,
    claimedTokens,
    claimableTokens,
  } = props;
  const theme = useTheme();
  const { t } = useTranslation();

  const data = {
    series: [
      {
        label: 'Claimed tokens',
        color: theme.palette.secondary.main,
        data: (claimedTokens / totalVesting) * 100,
      },
      {
        label: 'Claimable tokens',
        color: theme.palette.primary.dark,
        data: (claimableTokens / totalVesting) * 100,
      },
      {
        label: 'Locked tokens',
        color: 'hsla(216, 12%, 84%, 1)',
        data: (lockedTokens / totalVesting) * 100,
      },
    ]
  };

  const chartOptions: ApexOptions = {
    chart: {
      background: 'transparent',
      stacked: false,
      toolbar: {
        show: false
      }
    },
    colors: data.series.map((item) => item.color),
    tooltip: {
      enabled: true,
      y: {
        formatter: function(val, { globals, seriesIndex }) {
          return `${numeral(globals.seriesPercent[seriesIndex][0]).format('0.00')}%`
        }
      }
    },
    dataLabels: {
      enabled: true,
      formatter: function(val) {
        return `${numeral(val).format('0.00')}%`
      }
    },
    fill: {
      opacity: 1
    },
    labels: data.series.map((item) => item.label),
    legend: {
      labels: {
        colors: theme.palette.text.secondary
      },
      show: true
    },
    stroke: {
      width: 0
    },
    theme: {
      mode: theme.palette.mode
    }
  };

  const chartSeries = data.series.map((item) => item.data);

  return (
    <Box>
      <Typography variant='h5' textAlign='center'>
        Token Distribution
      </Typography>

      <Box mt={4}>
        <Chart
          type="pie"
          height={240}
          series={chartSeries}
          options={chartOptions}
        />
      </Box>
    </Box>
  );
};
