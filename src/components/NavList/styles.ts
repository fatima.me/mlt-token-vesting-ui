import type { SxProps, Theme } from '@mui/material';

type S = SxProps<Theme>;

const navList: S = {
  alignItems: 'center',
  display: {
    md: 'flex',
    xs: 'none'
  }
}

const navListVertical: S = {
  display: 'flex',
  flexDirection: 'column',
  height: '100%',
  width: '100%',
  '& > *': {
    margin: '10px 0'
  }
}

export const styles = {
  navList,
  navListVertical
};
