import {
  Box,
} from "@mui/material";

import NavLink from "../NavLink";

/* types */
import { styles } from "./styles";

interface NavListProps {
  list: {
    title: string;
    href: string;
  }[];
  orientation?: "horizontal" | "vertical";
  underline?: "none" | "hover";
}

const NavList = ({list, orientation = 'horizontal', underline}: NavListProps) => {
  return (
    <Box sx={orientation === 'vertical' ? styles.navListVertical : styles.navList}>
      {list.map((item) => (
        <NavLink href={item.href} key={item.href} underline={underline}>
          {item.title}
        </NavLink>
      ))}
    </Box>
  );
};

export default NavList;
