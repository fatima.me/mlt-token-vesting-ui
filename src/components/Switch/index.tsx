import React from 'react';
import { styled, Switch, SwitchProps, SxProps, Theme } from '@mui/material';

/* types */
import type { FC } from 'react';

interface SwitchCustomProps extends SwitchProps {
  labelChecked?: string;
  labelUnchecked?: string;
}

const SwitchCustomRoot = styled((props: SwitchCustomProps) => {
  const { labelChecked, labelUnchecked, ...rest} = props;
  return <Switch {...rest} />
})<SwitchCustomProps>((props) => {
  const {
    labelChecked = 'on',
    labelUnchecked = 'off'
  } = props;

  const styles: SxProps<Theme> = {
    width: 80,
    height: 48,
    padding: 8,
    '& .MuiSwitch-switchBase': {
      padding: 11,
      color: '#ff6a00',
    },
    '& .MuiSwitch-thumb': {
      width: 26,
      height: 26,
      backgroundColor: '#fff',
    },
    '& .MuiSwitch-track': {
      background: 'linear-gradient(to right, #43cea2, #185a9d)',
      opacity: '1 !important',
      borderRadius: 20,
      position: 'relative',
      '&:before, &:after': {
        display: 'inline-block',
        position: 'absolute',
        top: '50%',
        width: '50%',
        transform: 'translateY(-50%)',
        color: '#fff',
        textAlign: 'center',
      },
      '&:before': {
        content: `"${labelChecked}"`,
        left: 4,
        opacity: 0,
      },
      '&:after': {
        content: `"${labelUnchecked}"`,
        right: 4,
      },
    },
    '&.MuiSwitch-root': {
      '& .Mui-checked': {
        color: '#185a9d',
        backgroundColor: 'transparent',
        transform: 'translateX(32px)',
        '&:hover': {
          backgroundColor: 'rgba(24,90,257,0.08)',
        },
        '& .MuiSwitch-thumb': {
          backgroundColor: '#fff',
        },
        '+ .MuiSwitch-track': {
          background: 'linear-gradient(to right, #ee0979, #ff6a00)',
          '&:before': {
            opacity: 1,
          },
          '&:after': {
            opacity: 0,
          }
        },
      },
    }
  }

  return styles;
})

export const SwitchCustom: FC<SwitchCustomProps> = (props) => {
  return (
    <SwitchCustomRoot {...props} />
  )
}