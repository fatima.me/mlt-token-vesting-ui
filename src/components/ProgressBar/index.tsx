import { LinearProgress } from "@mui/material";

/* types */
import { styles } from "./styles";

interface ProgressBarProps {
  progress: number;
}

const ProgressBar = ({ progress }: ProgressBarProps) => {
  return (
    <LinearProgress variant="determinate" value={progress} sx={styles.progressBar}/>
  )
}

export default ProgressBar;
