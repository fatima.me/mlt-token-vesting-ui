import Dayjs from 'dayjs';
import { useTranslation } from 'react-i18next';
import {
  Box,
  Container,
  Grid,
  Typography
} from '@mui/material';

import { Logo } from '../logo';
import { styles } from './styles';
import { SITE_TITLE } from 'src/constants';
import { FacebookIcon } from 'src/icons/FacebookIcon';

/* icons */
import { TwitterIcon } from 'src/icons/TwitterIcon';
import { LinkedinIcon } from 'src/icons/LinkedinIcon';
import { TelegramIcon } from 'src/icons/TelegramIcon';
import { InstagramIcon } from 'src/icons/InstagramIcon';

/* types */
import type { FC, ReactNode } from 'react';

const Link: FC<{ href: string; children?: ReactNode; }> = ({ href, children }) => (
  <Box component='a' target='_blank' href={href} >
    {children}
  </Box>
)

const Footer: FC = (props) => {
  const { t } = useTranslation();

  return(
    <Box sx={styles.wrapperFooter} {...props} >
      <Grid container spacing={3} sx={styles.containerFooter}>
        <Grid item xs={12}>
          <Logo logotipoColor='#FFFFFF' />
        </Grid>

        <Grid item xs={12}>
          <Box sx={styles.boxSocial}>
            {/* <Link href='https://www.facebook.com/MintlayerMLT'>
              <FacebookIcon/>
            </Link> */}

            <Link href='https://twitter.com/mintlayer'>
              <TwitterIcon/>
            </Link>

            {/* <Link href='https://www.instagram.com/mintlayer'>
              <InstagramIcon/>
            </Link> */}

            <Link href='https://www.linkedin.com/company/mintlayer'>
              <LinkedinIcon/>
            </Link>

            <Link href='https://t.me/mintlayer'>
              <TelegramIcon/>
            </Link>
          </Box>
        </Grid>

        <Grid item xs={12}>
          <Typography variant="caption" >
            {`${Dayjs().year()} ${SITE_TITLE} © ${t('is designed by')} RBB S.r.l. - C.O.E. `}
            SM 28251
          </Typography>

          <Box/>

          <Typography variant="caption" >
            Certified as High Technology Entrerprise by San Marino innovation.
          </Typography>

          <Box/>

          <Typography variant="caption" >
            All Rights Reserved.
          </Typography>
        </Grid>
      </Grid>
    </Box>
  )
};

export default Footer;