import type { SxProps, Theme } from "@mui/material";

type S = SxProps<Theme>;

const wrapperFooter: S = {
  backgroundColor: 'secondary.main',
  backgroundImage: 'url(/static/assets/bg_footer.svg)',
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover',
  borderTopColor: 'divider',
  borderTopStyle: 'solid',
  color: '#FFFFFF',
  borderTopWidth: 1,
  pb: 1,
  pt: {
    md: 8,
    xs: 8
  }
};

const containerFooter: S = {
  display: 'grid',
  textAlign: 'center',
  justifyContent: 'center',
  marginBottom:10
};

const boxSocial: S = {
  my: 10,
  gridGap: 17,
  display: 'grid',
  justifyContent: 'center',
  gridTemplateColumns: 'repeat(auto-fit, 30px)',
  'svg': {
    width: 30,
    height: 'auto',
  }
};

export const styles = {
  wrapperFooter,
  containerFooter,
  boxSocial
};