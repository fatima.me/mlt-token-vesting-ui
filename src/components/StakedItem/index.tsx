import numeral from "numeral";
import {Link, Box, Typography, Button} from "@mui/material";
import { HIDDEN_LABEL } from "src/constants";
import LockIcon from '@mui/icons-material/Lock';
import LockOpenIcon from '@mui/icons-material/LockOpen';

/* types */
import { styles } from "./styles";

interface StakedItemProps {
  item: {
    balance: number;
    lockedUntil?: string;
    link: string;
  },
  hidden: boolean;
}

const StakedItem = ({item, hidden}: StakedItemProps) => {
  const title = item.lockedUntil ? `Locked until ${item.lockedUntil}:` : ' Without locking period:';
  return (
    <Box sx={styles.stakedItem}>
      <Box sx={styles.content}>
        {item.lockedUntil ? <LockIcon sx={styles.lockIcon}/> : <LockOpenIcon sx={styles.lockIcon}/> }
        <Box>
        <Typography variant="caption">
          {title}
        </Typography>
        <Typography variant="h5">
          {hidden ? HIDDEN_LABEL : `${numeral(item.balance).format('0,000.00')} ML`}
        </Typography>
        </Box>
        
      </Box>
     
      <Link href={item.link} target="_blank" underline="none">
        <Button variant="outlined">
          View on explorer
        </Button>
      </Link>
    </Box>
  )
}

export default StakedItem;
