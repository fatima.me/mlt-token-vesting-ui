import type { SxProps, Theme } from '@mui/material';

type S = SxProps<Theme>;

const stakedItem: S = {
  display: 'flex',
  flexDirection: {
    xs: 'column',
    sm: 'row',
  },
  justifyContent: 'space-between',
  alignItems: 'center',
  py: 2,
  borderBottom: '0.5px solid #fff',
  '&:last-child': {
    borderBottom: 'none',
  },
};

const lockIcon: S = {
  width: 20,
  height: 20,
  mr: 1,
  color: 'primary.dark',
};

const content: S = {
};

export const styles = {
  stakedItem,
  lockIcon,
  content
};
