import { useContext } from 'react';
import ReactDOM from 'react-dom';
import { Drawer, IconButton } from '@mui/material';
import { Logo } from "../logo";
import { CloseIcon } from 'src/icons/CloseIcon';
import { AppContext } from "src/contexts/app.context";

/* types */
import { styles } from "./styles";

interface SideBarProps {
  children?: React.ReactNode;
}

const SideBar = ({ children }: SideBarProps) => {
  const { sideBarOpen, setSideBarOpen } = useContext(AppContext);
  const handleClose = () => {
    setSideBarOpen(false);
  };

  return (
    ReactDOM.createPortal(
      <Drawer
        anchor={'right'}
        open={sideBarOpen}
        onClose={handleClose}
        sx={styles.drawer}
        PaperProps={{
          sx: styles.drawerPaper,
        }
      }
      >
        {children}
        <Logo sx={styles.logo} />
        <IconButton sx={styles.closeButton} aria-label="close slider menu" onClick={handleClose}>
          <CloseIcon />
        </IconButton>
      </Drawer>,
      document.body,
  ));
};

export default SideBar;



