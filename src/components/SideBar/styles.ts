import type { SxProps, Theme } from '@mui/material';

type S = SxProps<Theme>;

const drawer: S = {
  zIndex: 9999,
};

const drawerPaper: S = {
  width: {
    xs: '100%',
    sm: '350px',
  },
  padding: '100px 32px',
};

const closeButton: S = {
  position: 'absolute',
  top: 25,
  right: 25,
  width: 30,
  height: 30,
  cursor: 'pointer',
};


const logo: S = {
  position: 'absolute',
  top: 23,
  left: 16,
};

export const styles = {
  drawer,
  drawerPaper,
  closeButton,
  logo
};
