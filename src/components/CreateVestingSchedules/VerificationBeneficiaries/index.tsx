import numeral from 'numeral';
import { BigNumber } from 'ethers';
import { writeFileXLSX, utils as xlsxUtils } from 'xlsx';
import { formatEther, parseEther } from 'ethers/lib/utils';
import { useMemo, useCallback, useRef, useEffect, useState } from 'react';
import {
  Box,
  Button,
  Grid,
  LinearProgress,
  Skeleton,
  Typography
} from '@mui/material';

import { toastError } from 'src/utils/toastError';
import useUpdateEffect from 'src/hooks/useUpdateEffect';
import { getDecimalsFormat } from 'src/utils/getDecimalsFormat';
import BeneficiariesTable from '../BeneficiariesTable/BeneficiariesTable';

/* type */
import type { FC, ChangeEvent } from 'react';
import type { Beneficiaries, VerificationBeneficiariesProps } from 'src/types/vesting';

const tError = (msg: string) => toastError(msg, 7000);

export const VerificationBeneficiaries: FC<VerificationBeneficiariesProps> = (props) => {
  const {
    onBack = () => {},
    onContinue = () => {},
    supply,
    beneficiaries,
    setBeneficiaries,
  } = props;
  const [ loading, setLoading ] = useState(false);
  const uploadChangeRef = useRef<HTMLInputElement>();

  const worker = useMemo(() => {
    return new Worker(new URL('/src/workers/processBeneficiaries.ts', import.meta.url));
  }, [])

  useEffect(() => {
    worker.onmessage = (event: MessageEvent<Beneficiaries[]>) => {
      const beneficiaries = event.data;

      if(!beneficiaries.length) {
        resetSelectedFile();

        toastError(
          'The CSV file you uploaded is empty or the format is wrong. Please download our ' +
          'sample CSV file, use it as a guide, and reload beneficiary data',
          7000
        )
      }

      setLoading(false);
      setBeneficiaries(beneficiaries);
    }
  }, [ worker ])

  useUpdateEffect(() => {
    if(!beneficiaries.length) {
      tError('There is no beneficiary data. Please upload a non-empty CSV file.')
    }
  }, [ beneficiaries ])

  let totalTokensToDistribute = useMemo(() => {
    let total = BigNumber.from(0);

    for(const { amount } of beneficiaries) {
      total = total.add(parseEther(amount));
    }

    return total;
  }, [ beneficiaries ]);

  const handleContinue = useCallback(() => {
    if(supply.gt(totalTokensToDistribute)) {
      return tError('Total tokens to be distributed are greater than the available supply');
    }

    if(supply.lt(totalTokensToDistribute)) {
      return tError('Total tokens to be distributed are less than the available supply');
    }

    onContinue();
  }, [ supply, totalTokensToDistribute ])

  const openUploadFile = () => {
    uploadChangeRef.current.click();
  }

  const resetSelectedFile = () => {
    uploadChangeRef.current.value = '';
  }

  const uploadChanges = async (event: ChangeEvent<HTMLInputElement>) => {
    setLoading(true);

    try {
      const file = event.target.files[0];

      if(!file) {
        resetSelectedFile();
        return;
      }

      worker.postMessage({ file, beneficiaries });

    } catch(error) {
      setLoading(false);
      console.log('> error in uploadChanges', error);
      toastError(
        'An error occurred while processing your CSV file. Please verify that your file ' +
        'is correct and try again.',
        7000
      )
    }

    resetSelectedFile();
  }

  const handleExport = useCallback(() => {
    const workbook = xlsxUtils.book_new();

    const worksheet = xlsxUtils.json_to_sheet(beneficiaries.map(({ address, amount }) => {
      return { address, amount }
    }));

    worksheet["!cols"] = [
      { width: 70 },
      { width: 30 },
    ];

    xlsxUtils.book_append_sheet(workbook, worksheet, 'beneficiaries');

    writeFileXLSX(workbook, "vesting_tree.xlsx");
  }, [ beneficiaries ])

  const supplyStr = formatEther(supply);
  const supplyDecimals = supplyStr.split('.')[1];

  const formatDecimals =
    `0,000.${supplyDecimals ? getDecimalsFormat(supplyDecimals.length) : '000'}`;
  let supplyLabel = numeral(supplyStr).format(formatDecimals);

  return (
    <Grid container>
      <Grid item xs={12} mb={3}>
        <Typography variant='body1'>
          Review these details before generating the new TGE to be sent to the contract.
        </Typography>

        <Typography variant='body1'>
          In the next step, you will be able to see a preview of the entire vesting schedule.
        </Typography>
      </Grid>

      <Grid item xs={2}>
        <Typography variant='body1'>
          Beneficiaries
        </Typography>

        <Typography variant='h5'>
          {beneficiaries.length}
        </Typography>
      </Grid>

      <Grid item xs={3}>
        <Typography variant='body1'>
          Total available balance
        </Typography>

        {supplyLabel ? (
          <Typography variant='h5'>
            {supplyLabel}
          </Typography>
        ) : (
          <Skeleton sx={{ display: 'inline-block', width: 100 }} />
        )}
      </Grid>

      <Grid item xs={3}>
        <Typography variant='body1'>
          Total tokens to distribute
        </Typography>

        <Typography variant='h5'>
          {numeral(formatEther(totalTokensToDistribute)).format(formatDecimals)}
        </Typography>

        {(supply.lt(totalTokensToDistribute)) ? (
          <Typography variant='caption' color='error'>
            Total tokens to be distributed is greater than the available balance
          </Typography>
        ) : supply.gt(totalTokensToDistribute) ? (
          <Typography variant='caption' color='error'>
            Total tokens to be distributed less than the available balance
          </Typography>
        ) : null }
      </Grid>

      <Grid item xs={4}>
        <Box sx={{
          gridGap: 10,
          display: 'grid',
          gridTemplateColumns: 'max-content max-content',
          justifyContent: 'flex-end'
        }}>
          <Button variant='contained' onClick={handleExport}>
            Export as CSV
          </Button>

          <Box>
            <Button variant='contained' onClick={openUploadFile}>
              Upload changes
            </Button>

            <Box
              type='file'
              component='input'
              accept='.xlsx,.xls,.csv'
              sx={{ display: 'none' }}
              ref={uploadChangeRef}
              onChange={uploadChanges}
            />
          </Box>
        </Box>
      </Grid>

      {loading && (
        <Grid item xs={12} mt={4} mb={2}>
          <Typography variant='subtitle1' textAlign='center'>
            Data is being updated. This may take a while, be patient and don't close this window.
          </Typography>

          <LinearProgress/>
        </Grid>
      )}

      <Grid item xs={12} my={4}>
        <BeneficiariesTable
          beneficiaries={beneficiaries}
          setBeneficiaries={setBeneficiaries}
        />
      </Grid>

      <Grid item xs={6}>
        <Button variant='contained' onClick={onBack}>
          Back
        </Button>
      </Grid>

      <Grid item xs={6} >
        <Box textAlign='right'>
          <Button
            variant='contained'
            onClick={handleContinue}
            disabled={!beneficiaries.length}
          >
            continue
          </Button>
        </Box>
      </Grid>
    </Grid>
  )
}