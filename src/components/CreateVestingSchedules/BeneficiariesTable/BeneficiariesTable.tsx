import numeral from 'numeral';
import { useState } from 'react';
import { Delete } from "@mui/icons-material";
import {
  Table,
  TableRow,
  TableHead,
  TableCell,
  TableBody,
  IconButton,
  TablePagination,
} from "@mui/material";

/* types */
import type { FC, ChangeEvent } from 'react';
import type { Beneficiaries } from 'src/types/vesting';
import type { BeneficiariesTableProps } from "src/types/vesting";

function applyPagination(items: Beneficiaries[], page: number, rowsPerPage: number) {
  return items.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
};

const BeneficiariesTable: FC<BeneficiariesTableProps> = (props) => {
  const {
    beneficiaries,
    setBeneficiaries
  } = props;
  const [ page, setPage ] = useState(0);
  const [ rowsPerPage, setRowsPerPage ] = useState(10);

  const handleRowsPerPageChange = async (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
  }

  const handleDelete = (address_: string) => {
    setBeneficiaries((prevState) => prevState.filter(({ address }) => address_ != address));
  }

  const paginatedBeneficiaries = applyPagination(beneficiaries, page, rowsPerPage);

  return (
    <>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>
              Address
            </TableCell>

            <TableCell>
              Amount
            </TableCell>

            <TableCell width={130}/>
          </TableRow>
        </TableHead>

        <TableBody>
          {paginatedBeneficiaries.map(({ address, amount }, index) => {
            return(
              <TableRow key={`${index}-${address}`} hover>
                <TableCell>
                  {address}
                </TableCell>

                <TableCell>
                  {numeral(amount).format('0,000.00')}
                </TableCell>

                <TableCell>
                  <IconButton color='error' onClick={() => handleDelete(address)}>
                    <Delete/>
                  </IconButton>
                </TableCell>
              </TableRow>
            )
          })}
        </TableBody>
      </Table>

      <TablePagination
        page={page}
        component="div"
        rowsPerPage={rowsPerPage}
        count={beneficiaries.length}
        onPageChange={(_, page) => setPage(page)}
        rowsPerPageOptions={[ 5, 10, 25, 50, 100 ]}
        onRowsPerPageChange={handleRowsPerPageChange}
      />
    </>
  )
}

export default BeneficiariesTable;