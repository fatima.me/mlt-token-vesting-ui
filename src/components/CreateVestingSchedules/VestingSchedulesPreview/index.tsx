import dayjs from 'dayjs';
import numeral from 'numeral';
import { Contract } from 'ethers';
import { useRouter } from 'next/router';
import { toast } from 'react-hot-toast';
import { useEffect, useMemo, useState } from 'react';
import { formatEther, parseEther } from 'ethers/lib/utils';
import {
  Grid,
  Table,
  TableRow,
  TableHead,
  TableBody,
  TableCell,
  TablePagination,
  Button,
  Box,
  LinearProgress,
  Typography
} from "@mui/material"

import { ROUTES } from 'src/constants';
import { useWeb3 } from 'src/hooks/useWeb3';
import VestingAPI from 'src/lib/VestingAPI';
import { toastError } from 'src/utils/toastError';
import { MLTTokenABI } from 'src/contracts/MLTTokenABI';
import { MLTTokenAddress, VESTING_START_TIMESTAMP } from 'src/data/vesting_data_dapp';

/* types */
import type { FC, ChangeEvent } from "react";
import type { VestingSchedule } from '@mintlayer/vesting-tree/dist/types';
import type { MLTToken as IMLTToken } from 'src/types/contracts/MLTToken';
import type {
  VestingTreeData,
  VestingSchedulesPreviewProps,
  ProcessBeneficiariesResponse,
} from "src/types/vesting";

const tError = (msg:string) => toastError(msg, 7000);

function applyPagination(items: VestingSchedule[], page: number, rowsPerPage: number) {
  return items.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
};

export const VestingSchedulesPreview: FC<VestingSchedulesPreviewProps> = (props) => {
  const {
    supply,
    onBack,
    allocation,
    beneficiaries,
  } = props;
  const router = useRouter();
  const [ page, setPage ] = useState(0);
  const [ msgError, setMsgError] = useState('');
  const { addresses, provider, user } = useWeb3();
  const [ rowsPerPage, setRowsPerPage ] = useState(10);
  const [ vestingTreeData, setVestingTreeData ] = useState<VestingTreeData | null>(null);

  const connectedAddress = addresses[0];

  const handleRowsPerPageChange = async (event: ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
  }

  const worker = useMemo(() => {
    return new Worker(new URL('src/workers/generateVestingTree', import.meta.url));
  }, [])

  useEffect(() => {
    worker.onmessage = (event: MessageEvent<ProcessBeneficiariesResponse>) => {
      const { data } = event;

      const { status, message } = data;

      if(!status) {
        if(message) {
          setMsgError(message);
        }

        if(!message) {
          setMsgError(
            'An unexpected error occurred while trying to generate the TGE. ' +
            'Please refresh your browser (F5) and try again.'
          )
        }
      }

      if(status) {
        setVestingTreeData(data.data)
      }
    }
  }, [ worker ])

  useEffect(() => {
    try {
      const data = {
        allocation,
        beneficiaries,
        balance: supply.toString(),
        connectedAddress,
        vestingType: user.vestingType
      };

      worker.postMessage(data)
    } catch(error) {
      console.log('> error instantiating tree', error);
    }
  }, [ worker, beneficiaries, supply, allocation, connectedAddress, user ])

  const handleContinue = async () => {
    try {
      const MLTToken = (new Contract(MLTTokenAddress, MLTTokenABI, provider)) as IMLTToken;

      const {
        root,
        uriIPFS,
        proofBalance,
        vestingTypeProof,
        allocationQuantityProof
      } = vestingTreeData;

      const vestingDataStructs: IMLTToken.VestingDataStruct[] = [];

      const { vestingType, vestingSchedulesTreasury } = user;

      for(const vestingSchedule of vestingSchedulesTreasury) {
        const { amount, userAddress, vestingCliff, proof } = vestingSchedule;

        vestingDataStructs.push({
          amount,
          beneficiary: userAddress,
          cliff: vestingCliff,
          proof,
        })
      }

      const { unlocking, months, monthly, cliff } = vestingType;

      const allocation_: IMLTToken.AllocationStruct = {
        unlocking: parseEther(`${unlocking}`),
        months: months.map(month => parseEther(`${month}`)),
        monthly: monthly.map(month => parseEther(`${month}`)),
        cliff: parseEther(`${cliff}`)
      }

      const currentTree = user.trees.reduce((prev, current) => {
        return !prev ? current : prev.id < current.id ? current : prev;
      })

      const unsigneTx = await MLTToken.populateTransaction.addRoot(
        currentTree.root,
        root,
        supply,
        uriIPFS,
        allocation_,
        proofBalance,
        currentTree.treasuryAllocationProof[0].allocationTypeProof,
        vestingTypeProof,
        allocationQuantityProof,
        vestingDataStructs
      );

      const tx = await window.ethereum.request({
        method: "eth_sendTransaction",
        params: [{
          to: MLTTokenAddress,
          from: connectedAddress,
          data: unsigneTx.data
        }]
      });

      VestingAPI.setTreeLog(tx, root, connectedAddress);

      toast.success('Your transaction is being processed', { duration: 10000 });

      router.push(ROUTES.yourTokens);

    } catch(error) {
      console.log('> error in handleContinue', error);

      if(error.message) {
        switch (error.code) {
          case -32603: {
            tError("An error occurred while calling your wallet, please try again");
            break;
          }
          default: tError(error.message);
        }
      }

      if(!error.message) {
        tError('An unexpected error occurred. Please refresh your browser (F5).');
      }
    }
  }

  const {
    vestingSchedules = [],
  } = vestingTreeData || {};

  const paginatedBeneficiaries = applyPagination(vestingSchedules, page, rowsPerPage);

  return (
    <Grid container>
      {msgError ? (
        <Grid item xs={12} mb={4}>
          <Typography variant='subtitle1' textAlign='center' color='error.main'>
            {msgError}
          </Typography>
        </Grid>
      ) : (!vestingSchedules?.length) ? (
        <Grid item xs={12} mb={4}>
          <Typography variant='subtitle1' textAlign='center'>
            The TGE is being generated. This may take a while, please be patient and
            do not close this window
          </Typography>

          <LinearProgress/>
        </Grid>
      ) : (
        <>
        <Grid item xs={12} mb={2}>
          <Typography variant='subtitle1' textAlign='center' color='error.main'>
            Make sure to double check before passing to the next step.
          </Typography>

          <Typography variant='subtitle1' textAlign='center' color='error.main'>
            Once the transaction is submitted to the contract, the action cannot be reversed.
          </Typography>
        </Grid>

        <Grid item xs={12}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>
                  Date
                </TableCell>

                <TableCell>
                  Beneficiary
                </TableCell>

                <TableCell>
                  Tokens
                </TableCell>
              </TableRow>
            </TableHead>

            <TableBody>
              {paginatedBeneficiaries.map((item, index) => {
                const { address, amount, vestingCliff } = item;

                const cliff = VESTING_START_TIMESTAMP + vestingCliff;

                return(
                  <TableRow key={index}>
                    <TableCell>
                      {dayjs.unix(cliff).format('LLLL')}
                    </TableCell>

                    <TableCell>
                      {address}
                    </TableCell>

                    <TableCell>
                      {numeral(formatEther(amount)).format('0,000.00')}
                    </TableCell>
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>

          <TablePagination
            page={page}
            component="div"
            rowsPerPage={rowsPerPage}
            count={vestingSchedules.length}
            onPageChange={(_, page) => setPage(page)}
            rowsPerPageOptions={[ 5, 10, 25, 50, 100 ]}
            onRowsPerPageChange={handleRowsPerPageChange}
          />
        </Grid>
        </>
      )}

      <Grid item xs={6}>
        <Button variant='contained' onClick={onBack}>
          Back
        </Button>
      </Grid>

      <Grid item xs={6} >
        <Box textAlign='right'>
          <Button
            variant='contained'
            onClick={handleContinue}
            disabled={!vestingSchedules?.length}
          >
            Submit transaction
          </Button>
        </Box>
      </Grid>
    </Grid>
  )
}