import type { SxProps, Theme } from '@mui/material';

type S = SxProps<Theme>;

const uploadContainer: S = {
  gridGap: 10,
  display: 'grid',
  gridTemplateColumns: '100px 1fr'
};

const dummyImgCSV: S = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  borderWidth: 2,
  borderRadius: 1,
  borderStyle: 'solid',
  fontWeight: '900',
  minHeight: 120,
  maxHeight: 120,
  color: (t) => t.palette.primary.dark,
  borderColor: (t) => t.palette.primary.dark,
  backgroundColor: (t) => t.palette.primary.light,
};

const uploadDetailContainer: S = {
  display: 'grid',
  justifyContent: 'flex-start'
};

export const styles = {
  uploadContainer,
  dummyImgCSV,
  uploadDetailContainer,
};