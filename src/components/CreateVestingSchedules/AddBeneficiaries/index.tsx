import dayjs from "dayjs";
import numeral from "numeral";
import LinkNext from 'next/link';
import utc from "dayjs/plugin/utc";
import { randomHex } from 'web3-utils';
import { formatEther } from 'ethers/lib/utils';
import { useTranslation } from "react-i18next";
import { writeFileXLSX, utils as xlsxUtils } from 'xlsx';
import { useCallback, useRef, useState, useMemo, useEffect } from 'react';
import {
  Grid,
  Button,
  Box,
  Typography,
  Skeleton,
  LinearProgress
} from "@mui/material"

dayjs.extend(utc);

import { styles } from "./styles";
import { ROUTES } from "src/constants";
import { toastError } from "src/utils/toastError";
import { getDecimalsFormat } from 'src/utils/getDecimalsFormat';

/* types */
import type { FC, ChangeEvent } from 'react';
import type { AddBeneficiariesProps, Beneficiaries } from "src/types/vesting";

const tError = (msg: string, s = 7000) => toastError(msg, s);

export const AddBeneficiaries: FC<AddBeneficiariesProps> = (props) => {
  const {
    onContinue = () => {},
    supply,
    vestingType,
    beneficiaries,
    setBeneficiaries,
    vestingStartTimestamp,
  } = props;
  const { t } = useTranslation();
  const [ loading, setLoading ] = useState(false);
  const uploadFileRef = useRef<HTMLInputElement>();

  const worker = useMemo(() => {
    return new Worker(new URL('/src/workers/processBeneficiaries.ts', import.meta.url));
  }, [])

  useEffect(() => {
    worker.onmessage = (event: MessageEvent<Beneficiaries[]>) => {
      const beneficiaries = event.data;

      if(!beneficiaries.length) {
        resetSelectedFile();

        tError(
          'The CSV file you uploaded is empty, or the format is wrong. Please download our ' +
          'sample CSV file and use it as a model, then reload beneficiary data'
        )
      }

      setLoading(false);
      setBeneficiaries(beneficiaries);
    }
  }, [ worker ])

  const openUploadFile = () => {
    resetSelectedFile();
    uploadFileRef.current.click();
  }

  const resetSelectedFile = () => {
    setBeneficiaries([]);
    uploadFileRef.current.value = '';
  }

  const handleUploadBeneficiaries = async (event: ChangeEvent<HTMLInputElement>) => {
    setLoading(true);

    try {
      const file = event.target.files[0];

      if(!file) {
        resetSelectedFile();
        return;
      }

      worker.postMessage({ file });

    } catch(error) {
      setLoading(false);
      console.log('> error in handleUploadBeneficiaries', error);
      tError(
        'An error occurred while processing your CSV file. Please verify that your file is ' +
        'correct and try again.'
      )
    }
  }

  const handleDownload = useCallback(() => {
    const workbook = xlsxUtils.book_new();

    const usersDummy = [...new Array(50).keys()].map(() => {
      return {
        address: randomHex(20),
        amount: '200000',
      }
    })

    const worksheet = xlsxUtils.json_to_sheet(usersDummy);

    worksheet["!cols"] = [
      { width: 70 },
      { width: 30 },
    ];

    xlsxUtils.book_append_sheet(workbook, worksheet, vestingType);

    writeFileXLSX(workbook, "vesting_tree.xlsx");
  }, [ vestingType ])

  let supplyLabel = '';

  if(supply && supply.gt(0)) {
    const supplyStr = formatEther(supply);
    const supplyDecimals = supplyStr.split('.')[1];

    const formatDecimals =
      `0,000.${supplyDecimals ? getDecimalsFormat(supplyDecimals.length) : '000'}`;
    supplyLabel = numeral(supplyStr).format(formatDecimals);
  }

  return (
    <Grid container>
      <Grid item xs={12}>
        <Typography variant='body1'>
          Here's a template that has been generated with your vesting details in place. Download the template and add your investor addresses and allocation sizes.
        </Typography>
      </Grid>

      <Grid item xs={12} mt={2}>
        <Typography variant='h4'>
          Download CSV and enter beneficiaries
        </Typography>
      </Grid>

      <Grid item xs={12} mt={1}>
        <Typography variant='body1' fontWeight={600} mt={1}>
          Available balance:
          {' '}
          {supplyLabel ? (
            <Typography variant='body1' component='span'>
              {supplyLabel}
            </Typography>
          ) : (
            <Skeleton sx={{ display: 'inline-block', width: 100 }} />
          )}
        </Typography>

        <Typography variant='body1' fontWeight={600} mt={1}>
          Vesting type:
          {' '}
          <Typography variant='body1' component='span'>
            {vestingType}
          </Typography>
        </Typography>

        <Box my={2}/>

        <Button variant='contained' onClick={handleDownload}>
          Download CSV
        </Button>
      </Grid>

      <Grid item xs={12} my={4}>
        <Typography variant='h4'>
          Upload completed CSV
        </Typography>

        <Box sx={styles.uploadContainer}>
          <Box sx={styles.dummyImgCSV}>
            CSV
          </Box>

          <Box sx={styles.uploadDetailContainer}>
            <Typography variant='body1'>
              Once you've added investor addresses and allocation sizes, upload CSV.
            </Typography>

            <Typography variant='body1'>
              You can complete an error check in the next step.
            </Typography>

            <Box my={1} />

            <Box>
              <Button variant='contained' onClick={openUploadFile}>
                Select CSV
              </Button>

              <Box
                type='file'
                component='input'
                accept='.xlsx,.xls,.csv'
                sx={{ display: 'none' }}
                ref={uploadFileRef}
                onChange={handleUploadBeneficiaries}
              />
            </Box>
          </Box>
        </Box>
      </Grid>

      {loading && (
        <Grid item xs={12} mb={4}>
          <Typography variant='subtitle1' textAlign='center'>
            Data is being validated. This may take a while, be patient and don't close this window.
          </Typography>

          <LinearProgress/>
        </Grid>
      )}

      <Grid item xs={6}>
        <LinkNext href={ROUTES.yourTokens} passHref>
          <Button variant='contained'>
            Back
          </Button>
        </LinkNext>
      </Grid>

      <Grid item xs={6} >
        <Box textAlign='right'>
          <Button
            variant='contained'
            onClick={onContinue}
            disabled={!beneficiaries.length}
          >
            continue
          </Button>
        </Box>
      </Grid>
    </Grid>
  )
}