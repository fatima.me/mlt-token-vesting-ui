import { Contract } from "ethers";
import { useCallback, useEffect, useState } from "react";
import {
  Box,
  Button,
  Card,
  CardContent,
  Grid,
  Typography,
  useMediaQuery,
} from "@mui/material";

import Staked from "src/components/Staked";
import { useWeb3 } from "src/hooks/useWeb3";
import { EyeIcon } from "src/icons/EyeIcon";
import TotalPool from "src/components/TotalPool";
import { EyeOffIcon } from "src/icons/EyeOffIcon";
import RewardRatio from "src/components/RewardRatio";
import TotalBalance from "src/components/TotalBalance";
import { MLTTokenABI } from "src/contracts/MLTTokenABI";
import { MLTTokenAddress, MLT_SYMBOL } from "src/data/vesting_data_dapp";

/* types */
import type { FC } from "react";
import type { Theme } from "@mui/material";
import type { MLTToken as IMLTToken } from "src/types/contracts/MLTToken";
import { formatEther, parseEther } from "ethers/lib/utils";

export const MyStaking: FC = () => {
  const { addresses, provider } = useWeb3();
  const [ balance, setBalance ] = useState('');
  const [ hideNumbers, setHideNumbers ] = useState(false);
  const mdUp = useMediaQuery((theme: Theme) => theme.breakpoints.up('md'));

  const currentAddress = addresses[0];

  const getBalance = useCallback(async () => {
    try {
      const MLTToken = (new Contract(MLTTokenAddress, MLTTokenABI, provider)) as IMLTToken;

      const balance = await MLTToken.balanceOf(currentAddress);
      setBalance(formatEther(balance));

    } catch(error) {
      console.log('error in MyStaking getBalance', error);
    }
  }, [ currentAddress ] )
  
  
  useEffect(() => {
    getBalance();
  }, [ getBalance ])
  
  const handleHideNumbers = () => {
    setHideNumbers((prevState) => !prevState);
  }

  return (
    <Card>
      <CardContent>
        <Grid container alignItems='center' spacing={2}>
          <Grid item xs={7}>
            <Typography variant="h6">
              {`Your ${MLT_SYMBOL} Staking`}
            </Typography>
          </Grid>

          <Grid item xs={5} display='flex' justifyContent='flex-end'>
            <Button
              onClick={handleHideNumbers}
              startIcon={hideNumbers ? <EyeIcon/> : <EyeOffIcon/>}
            >
              {mdUp ? 'Hide numbers' : ''}
            </Button>
          </Grid>

          <Grid item xs={12} gap={6}>
              <TotalBalance balance={balance} hidden={hideNumbers} />

              <Staked amount={1000} hidden={hideNumbers}/>

              <TotalPool progress={50} />

              <RewardRatio progress={3} />
          </Grid>

          <Grid item xs={12}>
            <Button variant="contained" fullWidth>
              Stake
            </Button>

            <Box my={1}/>

            <Button fullWidth variant="outlined">
              Unstake
            </Button>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  )
}
