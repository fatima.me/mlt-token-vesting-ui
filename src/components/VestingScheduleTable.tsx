import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import numeral from 'numeral';
import NextLink from 'next/link';
import { Contract } from 'ethers';
import toast from 'react-hot-toast';
import debounce from 'just-debounce-it';
import { formatEther } from 'ethers/lib/utils';
import React, { useEffect, useCallback, useState } from 'react';
import {
  Box,
  Card,
  Grid,
  Link,
  Table,
  Button,
  TableRow,
  Checkbox,
  TextField,
  TableHead,
  TableCell,
  TableBody,
  Typography,
  InputAdornment,
  TablePagination,
  FormControlLabel,
  Divider,
  Skeleton,
} from '@mui/material';
import { LoadingButton } from '@mui/lab';

import { useWeb3 } from 'src/hooks/useWeb3';
import { SearchIcon } from 'src/icons/SearchIcon';
import { shortAddress } from 'src/utils/shortAddress';
import { MLTTokenABI } from 'src/contracts/MLTTokenABI';
import {
  BATCH_SIZE,
  MLTTokenAddress,
  NETWORK_EXPLORER_URL,
  VESTING_START_TIMESTAMP
} from 'src/data/vesting_data_dapp';

/* types */
import type { MLTToken } from 'src/types/contracts/MLTToken';
import type { VestingScheduleWithProof } from 'src/types/web3';
import type { FC, ChangeEvent, Dispatch, SetStateAction } from 'react';
import type {
  VestingScheduleTableProps,
  VestingScheduleAdminFilters,
} from 'src/types/vesting';

dayjs.extend(utc);

const SearchInput: FC<{
  setFilters: Dispatch<SetStateAction<VestingScheduleAdminFilters>>;
}> = ({ setFilters }) => {
  const [ search, setSearch ] = useState('');

  const setFilterDebouncer = useCallback(debounce((beneficiary: string) => {
    setFilters((prevState) => ({
      ...prevState,
      beneficiary
    }));
  }, 1000), [])

  const onChange = (event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    const { value } = event.target;

    setSearch(value);
    setFilterDebouncer(value);
  }

  return(
    <TextField
      fullWidth
      value={search}
      onChange={onChange}
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <SearchIcon fontSize="small" />
          </InputAdornment>
        ),
        sx: {
          '.MuiOutlinedInput-notchedOutline': {
            borderColor: (t) => t.palette.neutral[400],
          }
        }
      }}
      placeholder='Beneficiary search'
    />
  )
}

export const VestingScheduleTable: FC<VestingScheduleTableProps> = (props) => {
  const {
    isAdmin,
    filters,
    claiming,
    setFilters,
    setClaiming,
    isTreasurer,
    vestingSchedules,
    vestingSchedulesCount,
    onPageChange = () => {},
    onRowsPerPageChange = () => {},
    page,
    rowsPerPage
  } = props;
  const { addresses, provider } = useWeb3();
  const [ pendingClaims, setPendingClaims ] = useState<string[]>([]);
  const [ selectedVesting, setSelectedVesting ] = useState<string[]>([]);

  const connectedAddress = addresses[0];

  useEffect(() => {
    // Reset `pendingClaims` when `vestingsClaimed` change
    setPendingClaims([]);

    // Reset selected vesting when vestingSchedules change
    setSelectedVesting([]);
  }, [ JSON.stringify(vestingSchedules) ])

  const handleSelectAll = useCallback((event: ChangeEvent<HTMLInputElement>) => {
    const checked = event?.target?.checked;

    setSelectedVesting(!checked
      ? []
      : vestingSchedules.filter(({ vestingCliff, userAddress, vestingClaimed }) => {
        const disableForTreasury = isTreasurer &&
          connectedAddress.toLowerCase() == userAddress.toLowerCase();

        const cliff = VESTING_START_TIMESTAMP + vestingCliff;
        return !vestingClaimed && dayjs().isAfter(dayjs.unix(cliff)) && !disableForTreasury;
      })
      .map((vesting) => vesting.hash)
    );
  }, [ vestingSchedules, isTreasurer, connectedAddress ]);

  const handleSelectOne = useCallback((hash: string) => {
    if(!selectedVesting.includes(hash)) {
      setSelectedVesting((prevSelected) => [ ...prevSelected, hash ]);
    } else {
      setSelectedVesting((prevSelected) => prevSelected.filter((id) => id !== hash));
    }
  }, [ selectedVesting ])

  const handlerRelease = useCallback(async (data: VestingScheduleWithProof) => {
    setClaiming(true);
    const { userAddress, amount, vestingCliff, proof, hash, tree } = data;

    const { root } = tree;

    try {
      setPendingClaims((prevState) => ([ ...prevState, hash ]));
      const MLTToken = (new Contract(MLTTokenAddress, MLTTokenABI, provider)) as MLTToken;

      const unsigneTx = await MLTToken.populateTransaction.releaseVested(
        userAddress,
        amount,
        vestingCliff,
        root,
        proof
      );

      await window.ethereum.request({
        method: "eth_sendTransaction",
        params: [{
          to: MLTTokenAddress,
          from: connectedAddress,
          data: unsigneTx.data
        }]
      });
    } catch(error) {
      setPendingClaims((prevState) => prevState.filter((el) => el != hash));
      console.log('> error in handlerRelease:', error);

      if(error.message) {
        switch (error.code) {
          case -32603: {
            toast.error("An error occurred while calling your wallet, please try again");
            break;
          }
          default: toast.error(error.message);
        }
      }
    }

    setClaiming(false);
  }, [ provider ])

  const batchReleaseVested = useCallback(async () => {
    setClaiming(true);
    setPendingClaims((prevState) => prevState.concat(selectedVesting));

    try {
      const MLTToken = (new Contract(MLTTokenAddress, MLTTokenABI, provider)) as MLTToken;

      const users: MLTToken.VestingDataStruct[] = [];

      let root_ = '';
      const rootCount: { [root: string]: number } = {};

      vestingSchedules
        .slice(0, BATCH_SIZE)
        .filter(({ hash }) => selectedVesting.includes(hash))
        .forEach(({ amount, vestingCliff, proof, userAddress, tree }) => {

          const { root } = tree;

          if(!root_) {
            root_ = root;
          }

          if(rootCount.hasOwnProperty(root)) {
            rootCount[root] += 1;
          } else {
            rootCount[root] = 1;
          }

          users.push({ amount, beneficiary: userAddress, cliff: vestingCliff, proof });
        })

      if(Object.values(rootCount).length > 1) {
        toast.error('Cannot release tokens that do not belong to the same root');
        setClaiming(false);
        setPendingClaims((prevState) => prevState.filter((el) => !selectedVesting.includes(el)));
        return;
      }

      const unsigneTx = await MLTToken.populateTransaction.batchReleaseVested(
        users,
        root_
      );

      await window.ethereum.request({
        method: "eth_sendTransaction",
        params: [{
          to: MLTTokenAddress,
          from: connectedAddress,
          data: unsigneTx.data
        }]
      });

      setPendingClaims((prevState) => ([ ...prevState, ...selectedVesting ]));

    } catch (error) {
      console.log('> error in batchReleaseVested:', error);
      setPendingClaims((prevState) => prevState.filter((el) => !selectedVesting.includes(el)));

      if(error.message) {
        toast.error(error.message);
      }
    }

    setClaiming(false);
  }, [ selectedVesting, vestingSchedules, provider ])

  const enableBulkActions = selectedVesting.length > 0;

  const selectableVestingSchedule = vestingSchedules.filter((vesting) => {
    const { vestingCliff, vestingClaimed } = vesting;
    return !vestingClaimed && dayjs().isAfter(dayjs.unix(VESTING_START_TIMESTAMP + vestingCliff))
  })

  const selectedSome = selectedVesting.length > 0 &&
    selectedVesting.length < selectableVestingSchedule.length;

  const selectedAll = selectedVesting.length === selectableVestingSchedule.length;

  return (
    <Grid container>
      <Grid item xs={12} mb={4}>
        <Typography variant='h5'>
          Vesting Schedule
        </Typography>
      </Grid>

      <Grid item xs={12}>
        <Card>

          {isAdmin && (
            <>
              <Box sx={{ px: 3, pt: 3 }}>
                <FormControlLabel
                  control={
                    <Checkbox
                      checked={filters.unclaimed}
                      onChange={(_, checked) => {
                        setFilters((prevState) => ({
                          ...prevState,
                          unclaimed: checked
                        }))
                      }}
                    />
                  }
                  label={(
                    <Typography variant="body1">
                      Unclaimed
                    </Typography>
                  )}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      checked={filters.unlocked}
                      onChange={(_, checked) => {
                        setFilters((prevState) => ({
                          ...prevState,
                          unlocked: checked
                        }))
                      }}
                    />
                  }
                  label={(
                    <Typography variant="body1">
                      Unlockecd
                    </Typography>
                  )}
                />
              </Box>

              <Box sx={{ p: 3, pt: 1 }} >
                <SearchInput setFilters={setFilters}/>
              </Box>

              <Divider sx={{ my: 2, borderColor: (t) => t.palette.neutral[500] }} />
            </>
          )}

          <Box
            sx={{
              px: 2,
              py: 0.5,
              backgroundColor: 'neutral.100',
              display: !enableBulkActions && 'none',
            }}
          >
            <Checkbox
              checked={selectedAll}
              onChange={handleSelectAll}
              indeterminate={selectedSome}
            />

            <Button variant='contained' size="small" onClick={batchReleaseVested}>
              Batch Release Vesting
            </Button>
          </Box>

          <Table>
            <TableHead sx={{ visibility: enableBulkActions ? 'collapse' : 'visible' }} >
              <TableRow>
                <TableCell padding="checkbox">
                  <Checkbox
                    checked={selectedAll}
                    onChange={handleSelectAll}
                    indeterminate={selectedSome}
                  />
                </TableCell>

                <TableCell>
                  Date
                </TableCell>

                <TableCell>
                  root
                </TableCell>

                <TableCell>
                  Beneficiary
                </TableCell>

                <TableCell>
                  Claimed tokens
                </TableCell>

                <TableCell>
                  Claimable tokens
                </TableCell>

                <TableCell>
                  Locked tokens
                </TableCell>

                <TableCell/>
              </TableRow>
            </TableHead>

            <TableBody>
              {!vestingSchedules.length ? (
                <TableRow>
                  <TableCell colSpan={8}>
                    <Skeleton variant='rectangular'/>
                  </TableCell>
                </TableRow>
              ): (vestingSchedules.map((vesting) => {
                const {
                  hash,
                  tree,
                  amount,
                  userAddress,
                  vestingCliff,
                  vestingClaimed,
                  transactionHash,
                } = vesting;

                const { root } = tree;

                const cliff = VESTING_START_TIMESTAMP + vestingCliff;
                const isSelected = selectedVesting.includes(hash);

                const disableForTreasury = isTreasurer &&
                  connectedAddress.toLowerCase() == userAddress.toLowerCase();

                return(
                  <TableRow hover key={hash} selected={isSelected} >
                    <TableCell padding="checkbox">
                      <Checkbox
                        value={isSelected}
                        checked={isSelected}
                        onChange={() => handleSelectOne(hash)}
                        disabled={
                          vestingClaimed ||
                          disableForTreasury ||
                          !dayjs().isAfter(dayjs.unix(cliff))
                        }
                      />
                    </TableCell>

                    <TableCell>
                      {`${dayjs(cliff * 1000).format('LLLL')} (Local)`}
                    </TableCell>

                    <TableCell>
                      {shortAddress(root)}
                    </TableCell>

                    <TableCell>
                      <NextLink
                        passHref
                        href={`${NETWORK_EXPLORER_URL}/address/${userAddress}`}
                      >
                        <Link target='_blank'>
                          {shortAddress(userAddress)}
                        </Link>
                      </NextLink>
                    </TableCell>

                    <TableCell>
                      {numeral(formatEther(vestingClaimed ? amount : '0')).format('0,0.00')}
                    </TableCell>

                    <TableCell>
                      {
                        numeral(
                          formatEther(dayjs().isAfter(dayjs.unix(cliff)) ? amount : '0')
                        ).format('0,0.00')
                      }
                    </TableCell>

                    <TableCell>
                      {
                        numeral(
                          formatEther(dayjs().isAfter(dayjs.unix(cliff)) ? '0' : amount)
                        ).format('0,0.00')
                      }
                    </TableCell>

                    <TableCell>
                      {vestingClaimed ? (
                        <LoadingButton
                          target='_blank'
                          variant='outlined'
                          sx={{ minWidth: 160 }}
                          loading={!transactionHash}
                          href={`${NETWORK_EXPLORER_URL}/tx/${transactionHash}`}
                        >
                          View in explorer
                        </LoadingButton>
                      ) : (
                        <LoadingButton
                          variant='contained'
                          sx={{ minWidth: 160 }}
                          onClick={() => handlerRelease(vesting)}
                          loading={!!pendingClaims.find((el) => el == hash)}
                          disabled={
                            claiming ||
                            disableForTreasury ||
                            !dayjs().isAfter(dayjs.unix(cliff))
                          }
                        >
                          Release
                        </LoadingButton>
                      )}
                    </TableCell>
                  </TableRow>
                )
              }))}
            </TableBody>
          </Table>

          <TablePagination
            component="div"
            count={vestingSchedulesCount}
            onPageChange={(_, page) => onPageChange(page)}
            onRowsPerPageChange={onRowsPerPageChange}
            page={page}
            rowsPerPage={rowsPerPage}
            rowsPerPageOptions={[ 5, 10, 25, 50 ].filter((rows) => rows <= BATCH_SIZE)}
          />
        </Card>
      </Grid>
    </Grid>
  )
}