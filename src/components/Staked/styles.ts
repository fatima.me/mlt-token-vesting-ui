import type { SxProps, Theme } from '@mui/material';

type S = SxProps<Theme>;

const accordion: S = {
  mb: '3px',
  px: 2,
  py: 1,
  background: `
  linear-gradient(
    104.04deg,
    hsla(170, 80%, 33%, 1),
    hsla(170, 80%, 33%, 0.6) .01%,
    hsla(170, 80%, 33%, 0.1)
  ),
  hsla(170, 28%, 92%, 1)
  `,
  '& .MuiAccordionSummary-content': {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
  },
  '& .MuiButtonBase-root': {
    flexDirection: 'row',
  },
  '& .MuiAccordionDetails-root': {
    display: 'block',
    paddingBottom: 0,
    paddingTop: 0,
  },
  '& .MuiPaper-root': {
    borderRadius: 0,
  },
  '& .MuiAccordionSummary-gutters': {
    padding: 0,
  },

};

const summary: S = {
  display: 'flex',
  flexDirection: 'column',
};

export const styles = {
  summary,
  accordion,
};
