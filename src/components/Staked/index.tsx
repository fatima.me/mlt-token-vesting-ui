import numeral from "numeral";
import {Typography, Accordion, AccordionSummary, AccordionDetails} from "@mui/material";
import StakedItem from "src/components/StakedItem";
import { HIDDEN_LABEL } from "src/constants";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

/* types */
import { styles } from "./styles";

interface StakedProps {
  amount: number;
  hidden: boolean;
}

const Staked = ({amount, hidden}: StakedProps) => {
  const total = amount || 0;
  const STAKED_LIST = [
    {
      balance: 100,
      link: 'https://minlayer.org',
    },
    {
      balance: 100,
      lockedUntil: '10.10.2024',
      link: 'https://minlayer.org',
    },
    {
      balance: 100,
      lockedUntil: '07.08.2024',
      link: 'https://minlayer.org',
    },
  ]
  return (
    <Accordion sx={styles.accordion}>
      <AccordionSummary sx={styles.summary} expandIcon={<ExpandMoreIcon/>}>
        <Typography variant="caption">
          You totally staked:
        </Typography>
        <Typography variant="h5">
          {hidden ? HIDDEN_LABEL : `${numeral(total).format('0,000.00')} ML`}
        </Typography>
      </AccordionSummary>
   
      <AccordionDetails>
        {STAKED_LIST.map((item, index) => (
          <StakedItem key={index} item={item} hidden={hidden}/>
        ))}
      </AccordionDetails>
    </Accordion>
  )
}

export default Staked;
