import type { SxProps, Theme } from '@mui/material';

type S = SxProps<Theme>;

const progressBar: S = {
  width: '100%',
  height: 10,
  borderRadius: 0,
}

export const styles = {
  progressBar
};
