import type { SxProps, Theme } from '@mui/material';

type S = SxProps<Theme>;

const navLink: S = {
  display: 'flex',
  alignItems: 'center',
  padding: '0 5px',
  margin: '10px 5px',
  maxWidth: 'fit-content',
  transition: 'all 0.3s ease',
  color: 'text.tertiary',
  ":hover": {
    color: 'primary.main',
  }
}

const navLinkUnderline: S = {
  ":hover": {
    boxShadow: '0 2px 0 0 rgba(17,151,129, 0.5)',
    color: 'primary.main',
  }
}


export const styles = {
  navLink,
  navLinkUnderline
};
