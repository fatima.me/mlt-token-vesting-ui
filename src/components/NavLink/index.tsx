import { useContext } from "react";
import NextLink from "next/link";
import {Link} from "@mui/material";
import { AppContext } from "src/contexts/app.context";

/* types */
import { styles } from "./styles";

interface NavLinkProps {
  href: string;
  children: React.ReactNode;
  underline?: "none" | "hover";
}

const NavLink = ({ href, children, underline }: NavLinkProps) => {
  const { sideBarOpen, setSideBarOpen } = useContext(AppContext);

  const handleClick = () => {
    if (sideBarOpen) {
      setSideBarOpen(false);
    }
  };
  return (
    <NextLink href={href} passHref>
      <Link underline="none" sx={[styles.navLink, underline === 'hover' && styles.navLinkUnderline]} onClick={handleClick}>
        {children}
      </Link>
    </NextLink>
  );
};

export default NavLink;
