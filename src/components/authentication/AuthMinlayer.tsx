import { useEffect } from 'react';
import { useRouter } from 'next/router';

import { ROUTES } from 'src/constants';
import { useWeb3 } from 'src/hooks/useWeb3';
import { SplashScreen } from '../SplashScreen';

/* types */
import type { FC, ReactNode } from 'react';

interface AuthMinlayerProps {
  children: ReactNode;
}

export const AuthMinlayer: FC<AuthMinlayerProps> = (props) => {
  const { children } = props;
  const router = useRouter();
  const { isAuthenticated, isInitialized } = useWeb3();

  useEffect(() => {
    if(router.isReady) {
      if(isInitialized && !isAuthenticated) router.push({ pathname: ROUTES.index });
    }
  }, [ router.isReady, isAuthenticated, isInitialized ]);

  // If got here, it means that the redirect did not occur, and that tells us that the user is
  // authenticated / authorized.
  return isInitialized && isAuthenticated ? (<>{children}</>) : <SplashScreen/>;
};