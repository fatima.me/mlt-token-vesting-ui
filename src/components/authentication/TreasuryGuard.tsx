import { useEffect } from 'react';
import { useRouter } from 'next/router';

import { ROUTES } from 'src/constants';
import { useWeb3 } from 'src/hooks/useWeb3';

/* types */
import type { FC, ReactNode } from 'react';

interface TreasuryGuardProps {
  children: ReactNode;
}

export const TreasuryGuard: FC<TreasuryGuardProps> = (props) => {
  const { children } = props;
  const router = useRouter();
  const { user, isInitialized } = useWeb3();

  useEffect(() => {
    if(router.isReady) {
      if(isInitialized && !user.isTreasurer) router.push({ pathname: ROUTES.index });
    }
  }, [ router.isReady, user ]);

  return isInitialized && user.isTreasurer ? (<>{children}</>) : null;
};