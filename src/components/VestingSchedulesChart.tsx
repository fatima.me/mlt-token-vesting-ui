import numeral from 'numeral';
import { useTheme } from '@mui/material/styles';

import { Chart } from 'src/components/Chart';

/* types */
import type { FC } from 'react';
import type { ApexOptions } from 'apexcharts';
import type { VestingScheduleSchartProps } from 'src/types/vesting';

export const VestingSchedulesChart: FC<VestingScheduleSchartProps> = (props) => {
  const theme = useTheme();
  const { claimedTokensByDate = {}, claimableTokensByDate = {} } = props;

  const chartSeries = [
    {
      name: 'Claimable tokens',
      color: theme.palette.primary.main,
      data: Object.values(claimableTokensByDate),
    },
    {
      name: 'Claimed tokens',
      color: theme.palette.secondary.main,
      data: Object.values(claimedTokensByDate),
    }
  ]

  const chartOptions: ApexOptions = {
    chart: {
      type: 'area',
      toolbar: {
        show: false
      },
      zoom: {
        enabled: false
      }
    },
    dataLabels: {
      enabled: false
    },
    stroke: {
      curve: 'straight'
    },
    colors: chartSeries.map((item) => item.color),
    xaxis: {
      type: 'category',
      tickPlacement: 'on',
      categories: Object.keys(claimableTokensByDate),
      axisBorder: {
        color: theme.palette.divider,
        show: true
      },
      axisTicks: {
        show: false,
        color: theme.palette.divider,
      },
      labels: {
        offsetY: 5,
        style: {
          colors: theme.palette.text.secondary
        }
      }
    },
    yaxis: {
      opposite: false,
      title: {
        text: 'ML',
      },
      labels: {
        formatter: (val) => {
          return numeral(val).format('0,000.00');
        }
      },
      tickAmount: 14
    },
    legend: {
      horizontalAlign: 'left'
    },
    markers: {
      size: 6,
      strokeWidth: 0,
      strokeColors: theme.palette.background.default,
    },
    grid: {
      show: true,
      strokeDashArray: 0,
      borderColor: theme.palette.divider,
      xaxis: {
        lines: {
          show: true
        }
      },
      yaxis: {
        lines: {
          show: true
        }
      },
    },
    fill: {
      type: 'gradient'
    }
  };

  return (
    <Chart
      type="area"
      height={600}
      series={chartSeries}
      options={chartOptions}
    />
  );
};