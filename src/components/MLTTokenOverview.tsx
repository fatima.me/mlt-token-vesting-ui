import dayjs from 'dayjs';
import numeral from 'numeral';
import { useState } from 'react';
import NextLink from 'next/link';
import utc from 'dayjs/plugin/utc';
import { toast } from 'react-hot-toast';
import { LoadingButton } from '@mui/lab';
import {
  Link,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Typography,
  Box,
  Button
} from '@mui/material';

import { ROUTES } from '../constants';
import { ExternalLinkIcon } from 'src/icons/ExternalLinkIcon';
import {
  MLT_SYMBOL,
  MLT_DECIMAL,
  MLTTokenAddress,
  NETWORK_EXPLORER_URL,
  VESTING_START_TIMESTAMP,
} from 'src/data/vesting_data_dapp';

/* types */
import type { FC } from 'react';
import type { MLTTokenOverviewProps } from 'src/types/vesting';

dayjs.extend(utc);

export const MLTTokenOverview: FC<MLTTokenOverviewProps> = (props) => {
  const {
    address,
    tokensSource,
    allocationTypes,
    endDateTimestamp,
  } = props;
  const [ addingToken, setAddingToken ] = useState(false);

  const addToken = async () => {
    setAddingToken(true);

    try {
      const wasAdded = await window.ethereum.request({
        method: 'wallet_watchAsset',
        params: {
          type: 'ERC20', // Initially only supports ERC20, but eventually more!
          options: {
            address: MLTTokenAddress, // The address that the token is at.
            symbol: MLT_SYMBOL, // A ticker symbol or shorthand, up to 5 chars.
            decimals: MLT_DECIMAL, // The number of decimals in the token
            image: `${window.location.origin}/static/assets/MLT_TOKEN_IMAGEN.svg`, // A string url of the token logo
          },
        },
      });

      if(wasAdded) {
        toast.success('Token added successfully!');
      }

    } catch(error) {
      console.log('> error in addToken', error);

      if(error.code == 4001 && error.message) {
        toast.error(error.message);
      } else {
        toast.error('An error occurred while trying to add the Token');
      }
    }

    setAddingToken(false);
  }

  const { sources, total, claimable, claimed, locked } = tokensSource;

  return (
    <Table>
      <TableBody>
        <TableRow>
          <TableCell>
            <Typography variant="subtitle2">
              ML Token address
            </Typography>
          </TableCell>

          <TableCell>
            <Typography color="textSecondary" variant="body2" >
              <NextLink passHref href={`${NETWORK_EXPLORER_URL}/token/${MLTTokenAddress}`}>
                <Link target='_blank' sx={{display: 'flex', gridGap: 2, alignItems: 'center'}}>
                  <Typography variant='button'>
                    {MLTTokenAddress}
                  </Typography>
                  <ExternalLinkIcon fontSize='small' />
                </Link>
              </NextLink>
            </Typography>

            <Box mt={1}>
              <LoadingButton
                onClick={addToken}
                variant='contained'
                loading={addingToken}
              >
                Add token to wallet
              </LoadingButton>
            </Box>
          </TableCell>
        </TableRow>

        {/* <TableRow>
          <TableCell>
            <Typography variant="subtitle2">
              Balance available in wallet
            </Typography>
          </TableCell>

          <TableCell>
            <Typography color="textSecondary" variant="h5">
              {`${numeral(0). format('0,000.00')} ${MLT_SYMBOL}`}
            </Typography>

            <Box mt={1}>
              <NextLink href={ROUTES.staking} passHref>
                <Button variant='contained' component='a'>
                  Staking
                </Button>
              </NextLink>
            </Box>
          </TableCell>
        </TableRow> */}

        <TableRow>
          <TableCell>
            <Typography variant="subtitle2">
              Beneficiary
            </Typography>
          </TableCell>

          <TableCell>
            <NextLink
              passHref
              href={`${NETWORK_EXPLORER_URL}/token/${MLTTokenAddress}?a=${address}`}
            >
              <Link target='_blank' sx={{display: 'flex', gridGap: 2, alignItems: 'center'}}>
                <Typography variant='button'>
                  {address}
                </Typography>

                <ExternalLinkIcon fontSize='small'/>
              </Link>
            </NextLink>
          </TableCell>
        </TableRow>

        <TableRow>
          <TableCell>
            <Typography variant="subtitle2">
              Start date
            </Typography>
          </TableCell>

          <TableCell>
            <Typography
              color="textSecondary"
              variant="body2"
            >
              {`${dayjs.utc(VESTING_START_TIMESTAMP * 1000).format('LLL')} (UTC)`}
            </Typography>

            <Box m={1} />

            <Typography
              color="textSecondary"
              variant="body2"
            >
              {`${dayjs(VESTING_START_TIMESTAMP * 1000).format('LLL')} (Local)`}
            </Typography>
          </TableCell>
        </TableRow>

        <TableRow>
          <TableCell>
            <Typography variant="subtitle2">
              End date
            </Typography>
          </TableCell>

          <TableCell>
            <Typography
              color="textSecondary"
              variant="body2"
            >
              {`${dayjs.utc(endDateTimestamp * 1000).format('LLLL')} (UTC)`}
            </Typography>

            <Box m={1} />

            <Typography
              color="textSecondary"
              variant="body2"
            >
              {`${dayjs(endDateTimestamp * 1000).format('LLLL')} (Local)`}
            </Typography>
          </TableCell>
        </TableRow>

        <TableRow>
          <TableCell>
            <Typography variant="subtitle2">
              Pool name
            </Typography>
          </TableCell>

          <TableCell>

            <NextLink
              passHref
              href={`https://docs.mintlayer.org/whitepaper/6-token-and-public-sale#62-token-distribution--`}
            >
              {allocationTypes && !!allocationTypes.length ? (
                <Link
                  target='_blank'
                  sx={{
                    gridGap: 6,
                    alignItems: 'center',
                  }}
                >
                  {allocationTypes.map(({ type }, index, arr) => {
                    return(
                      <Box key={index} component='span'>
                        {`${type} ${index < arr.length - 1 ? ('|') : ''} `}
                      </Box>
                    )
                  })}

                  <Box component='span'>
                    <ExternalLinkIcon fontSize='small'/>
                  </Box>
                </Link>
              ): (
                <Typography variant="body2" color='textSecondary'>
                  N/A
                </Typography>
              )}
            </NextLink>
          </TableCell>
        </TableRow>

        {!!sources.length && (
          <>
            <TableRow>
              <TableCell>
                <Typography variant='subtitle2'>
                  Tokens source:
                </Typography>
              </TableCell>

              <TableCell>
                {sources.map(({ total, allocationType }) => {
                  return(
                    <Typography color="textSecondary" variant="body2" key={allocationType}>
                      {`${numeral(total). format('0,000.00')} ${allocationType}`}
                    </Typography>
                  )
                })}
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell>
                <Typography variant='subtitle2'>
                  Total tokens:
                </Typography>
              </TableCell>

              <TableCell>
                <Typography color="textSecondary" variant="body2">
                  {numeral(total).format('0,000.00')}
                </Typography>
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell>
                <Typography variant='subtitle2'>
                  Claimed tokens:
                </Typography>
              </TableCell>

              <TableCell>
                <Typography color="textSecondary" variant="body2">
                  {`${numeral(claimed).format('0,000.00')}`}
                  {' '}
                  {`(${numeral((claimed / total) * 100).format('0.00')}%)`}
                </Typography>
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell>
                <Typography variant='subtitle2'>
                  Claimable tokens:
                </Typography>
              </TableCell>

              <TableCell>
                <Typography color="textSecondary" variant="body2">
                  {`${numeral(claimable).format('0,000.00')}`}
                  {' '}
                  {`(${numeral((claimable / total) * 100).format('0.00')}%)`}
                </Typography>
              </TableCell>
            </TableRow>

            <TableRow>
              <TableCell>
                <Typography variant='subtitle2'>
                  Locked tokens:
                </Typography>
              </TableCell>

              <TableCell>
                <Typography color="textSecondary" variant="body2">
                  {`${numeral(locked).format('0,000.00')}`}
                  {' '}
                  {`(${numeral((locked / total) * 100).format('0.00')}%)`}
                </Typography>
              </TableCell>
            </TableRow>
          </>
        )}
      </TableBody>
    </Table>
  )
}