import type { SxProps, Theme } from '@mui/material';

type S = SxProps<Theme>;

const sideBarContent: S = {
  flexGrow: 1,
};

export const styles = {
  sideBarContent
};
