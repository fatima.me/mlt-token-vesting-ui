import NextLink from "next/link";
import { Box } from "@mui/material";
import NavList from "../NavList";
import Web3ModalButton from "../ConnectWallet/Web3ModalButton";
import { useWeb3 } from "src/hooks/useWeb3";

/* types */
import { styles } from "./styles";

interface SideBarContentProps {
  list: {
    title: string;
    href: string;
  }[];
}
const DEFAULT_NAV_LIST = [
  {
    title: "Home",
    href: "/",
  }
];

const SideBarContent = ({list}: SideBarContentProps) => {
  const { isAuthenticated, isInitialized } = useWeb3();
  return (
    <Box sx={styles.sideBarContent}>
      <NavList list={isAuthenticated && isAuthenticated ? list : DEFAULT_NAV_LIST} orientation="vertical" />
      {!isAuthenticated && 'Your wallet is not connected. Please connect your wallet to continue.'}
    </Box>
  );
};

export default SideBarContent;
