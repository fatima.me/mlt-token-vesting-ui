import numeral from "numeral";
import NextImage from "next/image";
import { Box, Card, CardContent, Grid, Typography, useMediaQuery } from "@mui/material";

import { GridContainer } from "./GridContainer";
import { MLT_SYMBOL } from "src/data/vesting_data_dapp";

/* types */
import type { FC } from "react";
import { SxProps, Theme } from "@mui/system";

const dividerVertical: SxProps<Theme> = {
  borderRightWidth: 1,
  borderRightStyle: 'solid',
  borderRightColor: 'neutral.400',
};

export const MLStats: FC = () => {
  const mdUp = useMediaQuery((theme: Theme) => theme.breakpoints.up('md'));
  
  return (
    <GridContainer>
      <Grid item xs={12}>
        <Card>
          <CardContent>
            <Grid
              container
              columnGap={3}
              rowSpacing={3}
              display={mdUp ? 'grid' : 'block'}
              gridTemplateColumns='repeat(3, 1fr)'
              gridTemplateAreas={`'header header header'`}
            >
              <Grid item gridArea='header'>
                <Typography variant='h6'>
                  {`${MLT_SYMBOL} Stats`}
                </Typography>
              </Grid>

              <Grid
                item
                sx={{
                  ...( mdUp ? dividerVertical : {}),
                  display: 'grid',
                  gridTemplateColumns: 'auto 1fr',
                  gridGap: 16,
                  verticalAlign: 'center'
                }}
              >
                <Box sx={{ display: 'flex', alignItems: 'center' }}>
                  <NextImage src='/favicon-32x32.png' width={32} height={32}/>
                </Box>
                <Box>
                  <Typography variant='caption'>
                    {`${MLT_SYMBOL} Price`}
                  </Typography>

                  <Typography variant='h5'>
                    {`$${numeral(10).format('0,000.00')}`}
                  </Typography>
                </Box>
              </Grid>

              <Grid item sx={mdUp ? dividerVertical : {}}>
                <Typography variant='caption'>
                  Daily rewards
                </Typography>
                
                <Typography variant='h5'>
                  {`$${numeral(389347).format('0,000.00')} MLPAD`}
                </Typography>
              </Grid>

              <Grid item>
                <Typography variant='caption'>
                  Circulating supply
                </Typography>
                
                <Typography variant='h5'>
                  {`$${numeral(400_000_000).format('0,000.00')} ${MLT_SYMBOL}`}
                </Typography>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Grid>
    </GridContainer>
  )
}
