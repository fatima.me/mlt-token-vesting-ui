import ProgressBar from "src/components/ProgressBar";
import { Box, Typography } from "@mui/material";
import PercentIcon from '@mui/icons-material/Percent';
import Card from "src/components/Card";

/* types */
import { styles } from "./styles";

interface RewardProps {
  progress: number;
}

const RewardRatio = ({ progress }: RewardProps) => {
  return (
    <Card>
      <Box sx={styles.ratio}>
        <PercentIcon sx={styles.balanceIcon}/> 
        <Box sx={styles.content}>
          <Typography variant="caption">
            Your reward ratio:
          </Typography>

          <Typography variant="h5" sx={styles.progress}>
            {progress}% 
          </Typography>
          <ProgressBar progress={progress} />
        </Box>
      </Box> 
    </Card>
  )
}

export default RewardRatio;
