import ProgressBar from "src/components/ProgressBar";
import { Box, Typography } from "@mui/material";
import AccountBalanceIcon from '@mui/icons-material/AccountBalance';
import Card from "src/components/Card";

/* types */
import { styles } from "./styles";

interface TotalPoolProps {
  progress: number;
}

const TotalPool = ({ progress }: TotalPoolProps) => {
  return (
    <Card>
      <Box sx={styles.total}>
        <AccountBalanceIcon sx={styles.balanceIcon}/> 
        <Box sx={styles.content}>
          <Typography variant="caption">
            Your part of the total pool:
          </Typography>

          <Typography variant="h5" sx={styles.progress}>
            {progress}% 
          </Typography>
          <ProgressBar progress={progress} />
        </Box>
      </Box>
      
    </Card>
  )
}

export default TotalPool;
