import type { SxProps, Theme } from '@mui/material';

type S = SxProps<Theme>;

const total: S = {
  display: 'flex',
  flexDirection: {
    xs: 'column',
    sm: 'row',
  },
  alignItems: {
    xs: 'flex-start',
    sm: 'center',
  },
}

const content: S = {
  width: '100%',
};

const balanceIcon: S = {
  width: 40,
  height: 40,
  mr: 2,
};

const progress: S = {
  mb: '5px',
};

export const styles = {
  total,
  content,
  balanceIcon,
  progress, 
};
