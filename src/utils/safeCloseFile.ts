import type { FileHandle } from "fs/promises";

export async function safeCloseFile(file?: FileHandle) {
  try {
    await file?.close();
  } catch(error) {
    console.log(error);
  }
}