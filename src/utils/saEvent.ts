export const saEvent = (eventName: string) => {
  try {
    if(window && window.sa_loaded && window.sa_event) return window.sa_event(eventName);
  } catch(error) {
    console.log('> error in saEvent', error);
  }
};