import toast from "react-hot-toast";
import { Box, Typography } from '@mui/material';

/* types */
import type { ReactNode } from 'react';

export const toastError = (msg: string | ReactNode, duration = 15000) => {
  return toast.error((t) => {
    return (
      <Box sx={{ cursor: 'pointer' }} onClick={() => toast.dismiss(t.id)}>
        {typeof msg == 'string' ? (
          <Box>
            <Typography variant='body1'>
              {msg}
            </Typography>
          </Box>
        ) : (
          msg
        )}
      </Box>
    )
  }, { duration })
}