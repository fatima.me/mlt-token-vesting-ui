export function getDecimalsFormat(unit: number) {
  return [...new Array(unit).keys()].map(() => '0').join('');
}