import { ThemeOptions } from '@mui/material';

const white =      `hsla(0, 0%, 100%, 1)`;
const neutralText =  `hsla(0, 0%, 44%, 1)`;
const lightMint =  `hsla(170, 28%, 92%, 1)`; /* light mint  similar-> hsla(172, 24%, 88%, 1) */
const mint =       `hsla(170, 80%, 33%, 1)`; /* mint #119781  similar green -> hsl(170, 80%, 33%); */
const darkMint =   `hsla(170, 52%, 16%, 1)`; /* sin usar */
const darkerMint =   `hsla(170, 53%, 10%, 1)`; /* similar text hsla(0, 0%, 0%, 1) */
const darkWhite =  `hsla(180, 25%, 98%, 1)`;
const black = `hsla(191, 14%, 23%, 1)`;

const neutral = {
  100: `hsla(220, 14%, 96%, 1)`,
  200: `hsla(220, 13%, 91%, 1)`,
  300: `hsla(216, 12%, 84%, 1)`,
  400: `hsla(218, 11%, 65%, 1)`,
  500: `hsla(220, 9%, 46%, 1)`,
  600: `hsla(215, 14%, 34%, 1)`,
  700: `hsla(215, 5%, 21%, 1)`,
  800: `hsla(215, 28%, 17%, 1)`,
  900: `hsla(221, 39%, 11%, 1)`,
};

const background = {
  default: white,
  light: lightMint, // '#e3f2ef',
  dark: mint,
  contrast: darkWhite,
  paper: white,
};

const divider = '#E6E8F0';

const primary = {
  main: '#119781', // mint 'hsla(170, 81%, 29%, 1)',
  light: lightMint, // '#e3f2ef',
  dark: '#119781',
  contrastText: white
};

const secondary = {
  main: darkerMint,
  light: mint,
  dark: darkerMint,
  contrastText: white
};

const success = {
  main: '#14B8A6',
  light: '#43C6B7',
  dark: '#0E8074',
  contrastText: white
};

const info = {
  main: '#2196F3',
  light: '#64B6F7',
  dark: '#0B79D0',
  contrastText: white
};

const warning = {
  main: '#FFB020',
  light: '#FFBF4C',
  dark: '#B27B16',
  contrastText: white
};

const error = {
  main: '#D14343',
  light: '#DA6868',
  dark: '#922E2E',
  contrastText: white
};

const text = {
  primary: darkerMint,
  secondary: neutralText,
  tertiary: black,
  disabled: 'rgba(55, 65, 81, 0.48)'
};

export const lightThemeOptions: ThemeOptions = {
  components: {
    MuiAvatar: {
      styleOverrides: {
        root: {
          backgroundColor: neutral[500],
          color: white
        }
      }
    },
    MuiChip: {
      styleOverrides: {
        root: {
          '&.MuiChip-filledDefault': {
            backgroundColor: neutral[200],
            '& .MuiChip-deleteIcon': {
              color: neutral[400]
            }
          },
          '&.MuiChip-outlinedDefault': {
            '& .MuiChip-deleteIcon': {
              color: neutral[300]
            }
          }
        }
      }
    },
    MuiInputBase: {
      styleOverrides: {
        input: {
          '&::placeholder': {
            opacity: 1,
            color: text.secondary
          },
        }
      }
    },
    MuiOutlinedInput: {
      styleOverrides: {
        notchedOutline: {
          borderColor: divider
        }
      }
    },
    MuiMenu: {
      styleOverrides: {
        paper: {
          borderColor: divider,
          borderStyle: 'solid',
          borderWidth: 1
        }
      }
    },
    MuiPopover: {
      styleOverrides: {
        paper: {
          borderColor: divider,
          borderStyle: 'solid',
          borderWidth: 1
        }
      }
    },
    MuiSwitch: {
      styleOverrides: {
        switchBase: {
          color: neutral[500]
        },
        track: {
          backgroundColor: neutral[400],
          opacity: 1
        }
      }
    },
    MuiTableCell: {
      styleOverrides: {
        root: {
          color:'inherit',
        }
      }
    },
    MuiTableHead: {
      styleOverrides: {
        root: {
          color: white,
          backgroundColor: background.light,
          '.MuiTableCell-root': {
            color: neutral[700]
          }
        }
      }
    },
    MuiButtonBase: {
      styleOverrides: {
        root: {
          '&.Mui-checked':{
            backgroundColor: background.light,
          },
        },
      }
    },
    MuiCard: {
      styleOverrides: {
        root: {
          backgroundColor: lightMint,
          borderRadius: 0,
        }
      }
    }
  },
  palette: {
    action: {
      active: neutral[500],
      focus: 'rgba(55, 65, 81, 0.12)',
      hover: background.light,
      selected: 'rgba(55, 65, 81, 0.08)',
      disabledBackground: 'rgba(55, 65, 81, 0.12)',
      disabled: 'rgba(55, 65, 81, 0.26)'
    },
    background,
    divider,
    error,
    info,
    mode: 'light',
    neutral,
    primary,
    secondary,
    success,
    text,
    warning
  },
  shadows: [
    'none',
    '0px 1px 1px rgba(100, 116, 139, 0.06), 0px 1px 2px rgba(100, 116, 139, 0.1)',
    '0px 1px 2px rgba(100, 116, 139, 0.12)',
    '0px 1px 4px rgba(100, 116, 139, 0.12)',
    '0px 1px 5px rgba(100, 116, 139, 0.12)',
    '0px 1px 6px rgba(100, 116, 139, 0.12)',
    '0px 2px 6px rgba(100, 116, 139, 0.12)',
    '0px 3px 6px rgba(100, 116, 139, 0.12)',
    '0px 2px 4px rgba(31, 41, 55, 0.06), 0px 4px 6px rgba(100, 116, 139, 0.12)',
    '0px 5px 12px rgba(100, 116, 139, 0.12)',
    '0px 5px 14px rgba(100, 116, 139, 0.12)',
    '0px 5px 15px rgba(100, 116, 139, 0.12)',
    '0px 6px 15px rgba(100, 116, 139, 0.12)',
    '0px 7px 15px rgba(100, 116, 139, 0.12)',
    '0px 8px 15px rgba(100, 116, 139, 0.12)',
    '0px 9px 15px rgba(100, 116, 139, 0.12)',
    '0px 10px 15px rgba(100, 116, 139, 0.12)',
    '0px 12px 22px -8px rgba(100, 116, 139, 0.25)',
    '0px 13px 22px -8px rgba(100, 116, 139, 0.25)',
    '0px 14px 24px -8px rgba(100, 116, 139, 0.25)',
    '0px 10px 10px rgba(31, 41, 55, 0.04), 0px 20px 25px rgba(31, 41, 55, 0.1)',
    '0px 25px 50px rgba(100, 116, 139, 0.25)',
    '0px 25px 50px rgba(100, 116, 139, 0.25)',
    '0px 25px 50px rgba(100, 116, 139, 0.25)',
    '0px 25px 50px rgba(100, 116, 139, 0.25)'
  ]
};
