/* types */
import type {
  SetAddressesAction,
  SetDisconnectAction,
  SetProviderAction,
  SetRegisterAction,
  Web3Handlers,
  Web3State,
  Web3StateAction,
} from "src/types/web3";

export const web3InitialState: Web3State = {
  user: null,
  addresses: [],
  provider: null,
  isInitialized: false,
  isAuthenticated: false,
  registerUser: async () => {},
  getFullInfoUser: async () => {},
};

const handlers: Web3Handlers = {
  SET_ADDRESSES: (state: Web3State, action: SetAddressesAction) => {
    const { addresses } = action.payload;
    return { ...state, addresses };
  },
  SET_PROVIDER: (state: Web3State, action: SetProviderAction) => {
    const { provider } = action.payload;
    return { ...state, provider };
  },
  SET_INITIALIZED: (state: Web3State) => {
    return { ...state, isInitialized: true };
  },
  REGISTER: (state: Web3State, action: SetRegisterAction) => {
    const { user } = action.payload;

    return {
      ...state,
      user,
      isAuthenticated: true
    };
  },
  DISCONNECT: (state: Web3State, action: SetDisconnectAction) => {
    return {
      ...state,
      user: null,
      addresses: [],
      isAuthenticated: false,
    }
  }
}

export const Web3Reducer = (state: Web3State, action: Web3StateAction): Web3State => (
  handlers[action.type] ? handlers[action.type](state, action) : state
);