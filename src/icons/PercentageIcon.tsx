import { createSvgIcon } from '@mui/material/utils';

export const PercentageIcon = createSvgIcon(
  <svg
    fill="none"
    height="24"
    stroke="currentColor"
    strokeLinecap="round"
    strokeLinejoin="round"
    strokeWidth="2"
    viewBox="0 0 24 24" width="24"
    xmlns="http://www.w3.org/2000/svg"
  >
    <g clipPath="url(#clip0_1808_1763)">
      <path d="M17 18C17.5523 18 18 17.5523 18 17C18 16.4477 17.5523 16 17 16C16.4477 16 16 16.4477 16 17C16 17.5523 16.4477 18 17 18Z" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
      <path d="M7 8C7.55228 8 8 7.55228 8 7C8 6.44772 7.55228 6 7 6C6.44772 6 6 6.44772 6 7C6 7.55228 6.44772 8 7 8Z" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
      <path d="M6 18L18 6" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" />
    </g>
    <defs>
      <clipPath id="clip0_1808_1763">
        <rect width="24" height="24" fill="white" />
      </clipPath>
    </defs>
  </svg>,
  'PercentageIcon'
);