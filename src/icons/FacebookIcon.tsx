import { styled } from '@mui/material';
import { useTheme } from '@mui/material/styles';

interface FacebookIconProps {
  iconColor?: string;
  circleColor?: string;
}

export const FacebookIcon = styled((props: FacebookIconProps) => {
  const theme = useTheme();
  const {
    circleColor = '#FFFFFF',
    iconColor = theme.palette.secondary.main,
    ...other
  } = props;

  return (
    <svg
      width="24"
      height="24"
      fill="none"
      viewBox="0 0 24 24"
      xmlns="http://www.w3.org/2000/svg"
      { ...other }
    >
      <path d="M12 24C18.6274 24 24 18.6274 24 12C24 5.37258 18.6274 0 12 0C5.37258 0 0 5.37258 0 12C0 18.6274 5.37258 24 12 24Z" fill={circleColor} />
      <path d="M15.0168 12.1852H12.8755V19.8511H9.63132V12.1852H8.08838V9.49113H9.63132V7.74775C9.63132 6.50104 10.2373 4.54883 12.9044 4.54883L15.3075 4.55865V7.17373H13.5639C13.2779 7.17373 12.8757 7.31336 12.8757 7.90808V9.49364H15.3002L15.0168 12.1852Z" fill={iconColor} />
    </svg>
  );
})``;