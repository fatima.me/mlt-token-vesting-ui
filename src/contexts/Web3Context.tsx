import toast from "react-hot-toast";
import { BigNumber, ethers } from "ethers";
import { Box, Typography } from '@mui/material';
import { useCallback, createContext, useEffect, useReducer } from "react";

import VestingAPI from 'src/lib/VestingAPI';
import { Web3TypeAction } from "src/types/web3";
import { web3InitialState, Web3Reducer } from "src/reducers/Web3Reducer";
import { connectWallet, getCurrentWalletConnected } from "src/lib/Web3Services";
/* types */
import type { FC } from "react";
import type { Web3ContextValues, Web3ProviderProps } from "src/types/web3";


export const Web3Context = createContext<Web3ContextValues>({
  ...web3InitialState,
  connectWallet: () => Promise.resolve(),
});

export const Web3ContextProvider: FC<Web3ProviderProps> = (props) => {
  const { children } = props;
  const [ state, dispatch ] = useReducer(Web3Reducer, web3InitialState);

  useEffect(() => {
    async function init() {
      const { status, data, hasProvider } = await getCurrentWalletConnected();

      if(status) {
        const { addresses } = data;

        if(addresses.length) {
          await register(addresses[0], hasProvider);
        }
      }

      dispatch({ type: Web3TypeAction.SET_INITIALIZED });
    }

    init();

    if(window.ethereum) {
      window.ethereum.on('connect', handleConnect);
      window.ethereum.on('disconnect', handleDisconnect);
      window.ethereum.on('chainChanged', handleChainChanged);
      window.ethereum.on('accountsChanged', handleAccountsChanged);
    }

    return () => {
      if(window.ethereum) {
        window.ethereum.removeListener('connect', handleConnect);
        window.ethereum.removeListener('disconnect', handleDisconnect);
        window.ethereum.removeListener('chainChanged', handleChainChanged);
        window.ethereum.removeListener('accountsChanged', handleAccountsChanged);
      }
    }
  }, []);

  const handleAccountsChanged = async (accounts: string[]) => {
    dispatch({ type: Web3TypeAction.DISCONNECT });

    if(accounts.length) {
      await register(accounts[0], true);
    }
  }

  const register = async (address: string, hasProvider: boolean) => {
    address && dispatch({
      type: Web3TypeAction.SET_ADDRESSES,
      payload: { addresses: [ address ] },
    });

    try {
      if(!hasProvider) {
        toast.error((
          <Box>
            <Typography>
              🦊 You must install Metamask, a virtual Ethereum wallet, in your browser.
              from https://metamask.io/download
            </Typography>
          </Box>
        ))
        return;
      }

      const provider = new ethers.providers.Web3Provider(window.ethereum);

      const { data, status } = await VestingAPI.getUser(address);

      if(hasProvider) {
        dispatch({
          type: Web3TypeAction.SET_PROVIDER,
          payload: { provider }
        })
      }

      if(status && data) {
        dispatch({
          type: Web3TypeAction.REGISTER,
          payload: { user: data }
        });
      }
    } catch(error) {
      console.log(error);
    }
  }

  const getFullInfoUser = useCallback(async () => {
    try {
      if(!state.provider || !state.addresses.length) return;

      const { data, status } = await VestingAPI.getUserFullInfo(state.addresses[0]);

      if(status && data) {
        dispatch({
          type: Web3TypeAction.REGISTER,
          payload: { user: data }
        });
      }
    } catch(error) {
      console.log(error);
    }
  }, [ state ])


  const handleChainChanged = (chainId: string) => {
    try {
      dispatch({
        type: Web3TypeAction.SET_PROVIDER,
        payload: { provider: new ethers.providers.Web3Provider(window.ethereum) }
      })
      console.log('handleChainChanged chainId', BigNumber.from(chainId).toNumber());
    } catch(error) {
      console.log('handleChainChanged error', error);
    }
  }

  const handleConnect = (connectInfo: { chainId: string }) => {
    console.log('handleConnect connectInfo', connectInfo);
  }

  const handleDisconnect = (error: any) => {
    console.log('handleDisconnect', error);
  }

  const hadleConnectWallet = async () => {
    const { data, message, status, hasProvider } = await connectWallet();

    if(status) {
      register(data.addresses[0], hasProvider);
    }

    if(!status) {
      message && toast.error(message);
    }
  }

  return (
    <Web3Context.Provider
      value={{
        ...state,
        getFullInfoUser,
        registerUser: register,
        connectWallet: hadleConnectWallet,
      }}
    >
      {children}
    </Web3Context.Provider>
  );
};