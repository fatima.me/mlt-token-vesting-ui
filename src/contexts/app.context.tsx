import { createContext, useState } from 'react';

interface AppContextProviderProps {
  value?: IAppContext;
  children: JSX.Element;
}

interface IAppContext {
  sideBarOpen: boolean;
  setSideBarOpen: (value: boolean) => void;
}

const defaultValues: IAppContext = {
  sideBarOpen: false,
  setSideBarOpen: () => {},
};

export const AppContext = createContext(defaultValues);

export const AppContextProvider = ({
  value: propValue,
  children,
}: AppContextProviderProps) => {
  const [sideBarOpen, setSideBarOpen] = useState<boolean>(false);

  const value = {
    sideBarOpen,
    setSideBarOpen,
  };

  return (
    <AppContext.Provider value={propValue || value}>
      {children}
    </AppContext.Provider>
  );
};
