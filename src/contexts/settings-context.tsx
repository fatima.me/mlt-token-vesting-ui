import { createContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';

/* types */
import type { FC } from 'react';
import {
  Settings,
  SettingsContextValue,
  SettingsProviderProps
} from 'src/types/settings';
import { LOCAL_STORAGE_KEYS } from 'src/constants';

const initialSettings: Settings = {
  theme: 'light',
  direction: 'ltr',
  responsiveFontSizes: true,
};

export const restoreSettings = (): Settings | null => {
  let settings = null;

  try {
    const storedData: string | null = window.localStorage.getItem(LOCAL_STORAGE_KEYS.settings);

    if (storedData) {
      settings = JSON.parse(storedData);
    } else {
      settings = {
        direction: 'ltr',
        responsiveFontSizes: true,
        theme: window.matchMedia('(prefers-color-scheme: dark)').matches
          ? 'dark'
          : 'light'
      };
    }
  } catch (err) {
    console.error(err);
    // If stored data is not a strigified JSON this will fail,
    // that's why we catch the error
  }

  return settings;
};

export const storeSettings = (settings: Settings): void => {
  window.localStorage.setItem(LOCAL_STORAGE_KEYS.settings, JSON.stringify(settings));
};

export const SettingsContext = createContext<SettingsContextValue>({
  saveSettings: () => {},
  settings: initialSettings,
});

export const SettingsProvider: FC<SettingsProviderProps> = (props) => {
  const { children } = props;
  const [ settings, setSettings ] = useState<Settings>(initialSettings);

  useEffect(() => {
    const restoredSettings = restoreSettings();

    if (restoredSettings) {
      setSettings(restoredSettings);
    }
  }, []);

  const saveSettings = (updatedSettings: Settings): void => {
    setSettings(updatedSettings);
    storeSettings(updatedSettings);
  };

  return (
    <SettingsContext.Provider
      value={{
        settings,
        saveSettings
      }}
    >
      {children}
    </SettingsContext.Provider>
  );
};

SettingsProvider.propTypes = {
  children: PropTypes.node.isRequired
};

export const SettingsConsumer = SettingsContext.Consumer;
