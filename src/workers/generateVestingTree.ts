import { parseEther } from "ethers/lib/utils";
import { VestingTree } from "@mintlayer/vesting-tree";

import { URL_API } from "src/constants";
import { VESTING_START_TIMESTAMP } from "src/data/vesting_data_dapp";

/* types */
import type { Beneficiaries, ProcessBeneficiariesResponse } from "src/types/vesting";
import { BigNumber, BigNumberish } from 'ethers';
import type {
  VestingType,
  VestingUsers,
  AllocationsType,
  VestingTreeParams,
} from "@mintlayer/vesting-tree/dist/types";

export interface ProcessBeneficiariesData {
  balance: BigNumberish;
  connectedAddress: string;
  vestingType: VestingType;
  allocation: AllocationsType;
  beneficiaries: Beneficiaries[];
}

self.onmessage = async (event: MessageEvent<ProcessBeneficiariesData>) => {
  const timerID = 'generateVestingTree:' + new Date();
  console.log('timerID:', timerID);
  console.time(timerID);

  const { vestingType, allocation, beneficiaries, connectedAddress } = event.data;

  const balanceStr = event.data.balance;
  const balance = BigNumber.from(balanceStr);

  let result: ProcessBeneficiariesResponse = {
    status: false,
    data: {
      root: '',
      uriIPFS: '',
      proofBalance: [],
      vestingSchedules: [],
      vestingTypeProof: [],
      allocationQuantityProof: []
    }
  };

  try {
    const users: VestingUsers[] = [];

    for(const { address, amount } of beneficiaries) {
      users.push({
        address,
        amount: parseEther(amount),
        allocationsType: allocation,
      })
    }

    const treasurerData = {
      address: connectedAddress,
      vestingType,
    }

    const vestingTreeParams: VestingTreeParams = {
      users,
      balance,
      vestingStartTimestamp: VESTING_START_TIMESTAMP,
      allocations: {
        [allocation]: {
          percentage: parseEther('1'),
          vestingInfo: vestingType,
        }
      },
      treasurers: [treasurerData],
      ownerAddress: connectedAddress
    }

    const tree = new VestingTree(vestingTreeParams);

    const bodyData = JSON.stringify({
      address: connectedAddress,
      root: tree.getHexRoot(),
      balance: balanceStr,
      beneficiaries
    });

    const response = await fetch(`${URL_API}/api/set-tree`, {
      method: 'POST',
      body: bodyData,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    });

    interface ResponseData {
      status: boolean;
      message: string;
      data: {
        uriIPFS: string;
      }
    }

    const data = await response.json() as ResponseData;

    const { status, message } = data;

    if(!status) {
      result.message = message;
      self.postMessage(result);
      return;
    }

    const proofBalance = tree.getHexProof(tree.balancehash(balance));

    const vestingTypeProof = tree.getHexProof(tree.treasurerHash(treasurerData));

    const allocationQuantityProof = tree.getHexProof(tree.allocationQuantityHash());

    if(status) {
      result = {
        status: true,
        data: {
          proofBalance,
          vestingTypeProof,
          root: tree.getHexRoot(),
          allocationQuantityProof,
          uriIPFS: data.data.uriIPFS,
          vestingSchedules: tree.vestingSchedules,
        }
      };
    }

  } catch(error) {
    console.log('> error in generateVestingTree worker', error);
  }

  console.timeEnd(timerID);
  self.postMessage(result);
}