import { read as xlsxRead, utils as xlsxUtils } from 'xlsx';
import { isAddress, parseEther, formatEther } from 'ethers/lib/utils';

/* types */
import type { Beneficiaries } from "src/types/vesting";

export interface ProcessBeneficiariesData {
  file: File;
  beneficiaries: Beneficiaries[] | null;
}

self.onmessage = async (event: MessageEvent<ProcessBeneficiariesData>) => {
  const beneficiaries: Beneficiaries[] = [];

  try {
    const fileBufer = await event.data.file.arrayBuffer();

    const wb = xlsxRead(fileBufer, { raw: true });
    const beneficiariesRaw: any[] = xlsxUtils.sheet_to_json(wb.Sheets[wb.SheetNames[0]]);

    const beneficiariesMap: { [address: string]: Beneficiaries } = {};

    if(event.data?.beneficiaries?.length) {
      for(const beneficiary of event.data?.beneficiaries) {
        const { address, amount } = beneficiary;

        if(beneficiariesMap.hasOwnProperty(address)) {
          const amountBN = parseEther(amount);
          beneficiariesMap[address].amount = `${parseFloat(
            formatEther(
              amountBN.add(
                parseEther(beneficiariesMap[address].amount)
              )
            )
          )}`;
        }

        if(!beneficiariesMap.hasOwnProperty(address)) {
          beneficiariesMap[address] = {
            amount,
            address,
          }
        }
      }
    }

    for(const item of beneficiariesRaw) {
      const amount = item?.amount;
      const address = item?.address;

      if(address && amount && !isNaN(parseFloat(amount)) && isAddress(address)) {
        if(beneficiariesMap.hasOwnProperty(address)) {
          const amountBN = parseEther(amount);
          beneficiariesMap[address].amount = `${parseFloat(
            formatEther(
              amountBN.add(
                parseEther(beneficiariesMap[address].amount)
              )
            )
          )}`;
        }

        if(!beneficiariesMap.hasOwnProperty(address)) {
          beneficiariesMap[address] = {
            amount,
            address,
          }
        }
      }
    }

    for(const key in beneficiariesMap) {
      beneficiaries.push({
        ...beneficiariesMap[key],
        amount: `${beneficiariesMap[key].amount}`
      });
    }
  } catch(error) {
    console.log('> error in processBeneficiaries worker', error);
  }

  self.postMessage(beneficiaries);
}