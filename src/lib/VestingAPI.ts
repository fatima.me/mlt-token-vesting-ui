import axios, { AxiosError } from 'axios';

import httpClient from './httpClient';

/* types */
import type { IResponseAPI } from 'src/types/commonAPI';
import type { User, VestingScheduleWithProof } from 'src/types/web3';
import type {
  AllocationWithTokenCount,
  getVestingSchedulesProps,
  VestingScheduleAdminFilters,
} from 'src/types/vesting';

class VestingAPI {
  async getUser(address: string) {
    const result: IResponseAPI<User | null> = {
      data: null,
      message: '',
      status: false,
    }

    try {
      const response = await httpClient.post({ url: `/user`, body: { address } });

      const data = response.data.data;
      const status = response.data.status;

      if(status) {
        result.data = data;
        result.status = status;
      }

      return result;

    } catch(error) {
      error as AxiosError;
      console.log('> error in getUser', error);

      const message = error?.response?.data?.message;

      result.message = 'An unexpected error occurred. Please try again later.';

      if(axios.isAxiosError(error) && message) {
        result.message = message;
      }

      return result;
    }
  }

  async getUserFullInfo(address: string) {
    const result: IResponseAPI<User | null> = {
      data: null,
      message: '',
      status: false,
    }

    try {
      const response = await httpClient.post({ url: `/user-fullinfo`, body: { address } });

      const data = response.data.data;
      const status = response.data.status;

      if(status) {
        result.data = data;
        result.status = status;
      }

      return result;

    } catch(error) {
      error as AxiosError;
      console.log('> error in getUserFullInfo', error);

      const message = error?.response?.data?.message;

      result.message = 'An unexpected error occurred. Please try again later.';

      if(axios.isAxiosError(error) && message) {
        result.message = message;
      }

      return result;
    }
  }

  async getVestingSchedules(params: (getVestingSchedulesProps & VestingScheduleAdminFilters)) {
    const result: IResponseAPI<{
      vestingSchedulesTotal: number;
      vestingSchedules: VestingScheduleWithProof[];
    }> = {
      data: null,
      message: '',
      status: false,
    }

    try {
      const response = await httpClient.post({ url: '/vesting-schedules', body: params });

      const data = response.data.data;
      const status = response.data.status;

      if(status) {
        result.data = data;
        result.status = status;
      }

      return result;

    } catch(error) {
      error as AxiosError;
      console.log('> error in getVestingSchedules', error);

      const message = error?.response?.data?.message;

      result.message = 'An unexpected error occurred. Please try again later.';

      if(axios.isAxiosError(error) && message) {
        result.message = message;
      }

      return result;
    }
  }

  async setTreeLog(tx: string, root: string, address: string) {
    try {
      await httpClient.post({ url: '/set-tree-logs', body: { tx, root, address } });
    } catch(error) {
      console.log('> error in setTreeLog', error);
    }
  }

  async deleteTreeLog(id: number, address: string) {
    try {
      await httpClient.post({ url: '/delete-tree-logs', body: { id, address } });
    } catch(error) {
      console.log('> error in deleteTreeLog', error);
    }
  }

  async getAllocation(address: string) {
    const result: IResponseAPI<AllocationWithTokenCount | null> = {
      data: null,
      message: '',
      status: false,
    }

    try {
      const response = await httpClient.post({ url: `/get-allocations`, body: { address } });

      const data = response.data.data;
      const status = response.data.status;

      if(status) {
        result.data = data;
        result.status = status;
      }

      return result;

    } catch(error) {
      error as AxiosError;
      console.log('> error in getAllocation', error);

      const message = error?.response?.data?.message;

      result.message = 'An unexpected error occurred. Please try again later.';

      if(axios.isAxiosError(error) && message) {
        result.message = message;
      }

      return result;
    }
  }

  async setLogs(address: string, data: any) {
    try {
      httpClient.post({ url: `/set-logs`, body: { address, data } });
    } catch(error) {
      error as AxiosError;
      console.log('> error in setLogs', error);
    }
  }
}

export default new VestingAPI();
