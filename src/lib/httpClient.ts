import axios from 'axios';
import { URL_API } from 'src/constants';

const URL_BASE = `${URL_API}/api`;

const readUrl = (url: string) => {
	return url.startsWith('http://') || url.startsWith('https://') ? url : `${URL_BASE}${url}`
}

const HEADERS_DEFAULT = {
	'Accept': 'application/json',
	'Content-Type': 'application/json',
	'Access-Control-Allow-Origin': '*',
}

const get = ({ url = '', options = {}, headers = {} }) => {
	return axios.get(readUrl(url), {
		headers: {
			...HEADERS_DEFAULT,
			...headers,
		},
		...options,
	});
}

const post = ({ url = '', body = {}, headers = {}, options = {} }) => {
	return axios.post(readUrl(url), body, {
		headers: {
			...HEADERS_DEFAULT,
			...headers,
		},
		...options
	})
}

export default {
	get,
	post,
}
