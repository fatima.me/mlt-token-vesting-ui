export const convertBase64 = (file: File): Promise<string | null> => {
  return new Promise((resolve) => {
    const fileReader = new FileReader();
    fileReader.readAsDataURL(file);
    fileReader.onload = () => {
      resolve((typeof fileReader.result === 'string') ? fileReader.result : null);
    };
    fileReader.onerror = () => {
      resolve(null);
    };
  });
};