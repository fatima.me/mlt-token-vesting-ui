import { ConnectWalletResult } from "src/types/web3";

export const connectWallet = async () => {
  const result: ConnectWalletResult = {
    data: null,
    message: '',
    status: false,
    hasProvider: false,
  };

  if(window.ethereum) {
    result.hasProvider = true;

    try {
      const addresses = await window.ethereum.request({ method: "eth_requestAccounts" });

      result.data = {
        addresses
      }

      result.status = true;

    } catch(error) {
      console.log(error);

      if(error?.message) {
        result.message = error.message
      }
    }
  }

  return result;
};

export const getCurrentWalletConnected = async () => {
  const result: ConnectWalletResult = {
    data: null,
    message: '',
    status: false,
    hasProvider: false,
  };

  if(window.ethereum) {
    result.hasProvider = true;

    try {
      const addresses = await window.ethereum.request({ method: "eth_accounts" });

      result.status = true;
      result.data = { addresses };

    } catch (error) {
      console.log(error);

      if(error?.message) {
        result.message = error.message
      }
    }
  }

  return result;
};