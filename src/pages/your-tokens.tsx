import Head from 'next/head';
import NextLink from 'next/link';
import io from 'socket.io-client';
import toast from 'react-hot-toast';
import debounce from 'just-debounce-it';
import { useCallback, useEffect, useState } from 'react';
import {
  Box,
  Grid,
  Button,
  Typography,
  Skeleton,
} from '@mui/material';

import VestingAPI from 'src/lib/VestingAPI';
import { saEvent } from 'src/utils/saEvent';
import { useWeb3 } from 'src/hooks/useWeb3';
import MainLayout from 'src/components/MainLayout';
import useUpdateEffect from 'src/hooks/useUpdateEffect';
import { globalStyles as styles } from 'src/styles/global';
import { ROUTES, SITE_TITLE, URL_API } from 'src/constants';
import { GridContainer } from 'src/components/GridContainer';
import { ValidateNetwork } from 'src/components/ValidateNetwork';
import { PoolTotalsTable } from 'src/components/PoolTotalsTable';
import { MLTTokenOverview } from 'src/components/MLTTokenOverview';
import { TokenDistribution } from 'src/components/TokenDistribution';
import { AuthMinlayer } from 'src/components/authentication/AuthMinlayer';
import { VestingScheduleTable } from 'src/components/VestingScheduleTable';
import { VestingSchedulesChart } from 'src/components/VestingSchedulesChart';

/* types */
import type { NextPage } from 'next';
import type { ChangeEvent } from 'react';
import type { VestingScheduleWithProof } from 'src/types/web3';
import type { ReloadVestingSchedules, VestingScheduleAdminFilters } from 'src/types/vesting';

const socket = io(URL_API);
const PAGE_TITLE = 'Your Tokens';
const ROWS_PER_PAGE = 10;
const FILTERS_DEFAULT = {
  beneficiary: '',
  unclaimed: false,
  unlocked: false,
};

async function applyFilters(filters: VestingScheduleAdminFilters, address: string) {
  return VestingAPI.getVestingSchedules({
    address,
    skip: 0,
    limit: ROWS_PER_PAGE * 4,
    ...filters
  })
}

function applyPagination(items: VestingScheduleWithProof[], page: number, rowsPerPage: number) {
  return items.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
};

const YourTokensPage: NextPage = () => {
  const [ claiming, setClaiming ] = useState(false);
  const [ filtering, setFiltering ] = useState(false);
  const { user, addresses, getFullInfoUser } = useWeb3();
  const [ rowsPerPage, setRowsPerPage ] = useState(ROWS_PER_PAGE);
  const [ vestingSchedulePage, setVestingSchedulePage ] = useState(0);
  const [ vestingSchedulesCount, setVestingSchedulesCount ] = useState(0);
  const [ filters, setFilters ] = useState<VestingScheduleAdminFilters>(FILTERS_DEFAULT);

  const [
    filteredVestingSchedules,
    setFilteredVestingSchedule
  ] = useState<VestingScheduleWithProof[]>([]);

  const [
    paginatedVestingSchedule,
    setPaginatedVestingSchedule
  ] = useState<VestingScheduleWithProof[]>([]);

  const connectedAddress = addresses[0];

  useEffect(() => {
    getFullInfoUser();
  }, [])

  const registerUserDebouncer = useCallback(debounce(async () => {
    getFullInfoUser();
  }, 1000), [ getFullInfoUser ])

  useEffect(() => {
    const { beneficiary, unclaimed, unlocked } = filters;
    setFiltering(!!beneficiary || unclaimed || unlocked);
  }, [ filters ])

  useEffect(() => {
    applyFilters(filters, connectedAddress)
    .then(({ data, status }) => {
      setVestingSchedulePage(0);

      if(status) {
        setFilteredVestingSchedule(data.vestingSchedules);
        setVestingSchedulesCount(data.vestingSchedulesTotal);
      }
    })
    .catch((error) => {
      console.log('error', error);
      toast.error('An unexpected error occurred. Please refresh your browser (F5).');
    })
  }, [ filters, filtering, connectedAddress ])

  useEffect(() => {
    function listenVestedTokenGrant(vestingSchedule: VestingScheduleWithProof) {
      setFilteredVestingSchedule((prevState) => {
        return prevState.map((item) => {
          const result = { ...item };

          if(item.hash == vestingSchedule.hash) {
            result.vestingClaimed = true;
            result.transactionHash = vestingSchedule.transactionHash;
            saEvent('vestingClaimed');
          }

          return result;
        })
      })

      registerUserDebouncer();
    }

    async function reloadVestingSchedules(data: ReloadVestingSchedules) {
      try {
        const { vestingSchedules, vestingSchedulesTotal } = data;

        if(user.isAdmin) {
          registerUserDebouncer();

          setVestingSchedulePage(0);
          setFilters(FILTERS_DEFAULT);
          setRowsPerPage(ROWS_PER_PAGE);
          setFilteredVestingSchedule(vestingSchedules);
          setVestingSchedulesCount(vestingSchedulesTotal);
        }
      } catch(error) {
        console.log('> Error in reloadVestingSchedules', error);
      }
    }

    socket.on('vestedTokenGrant', listenVestedTokenGrant);
    socket.on('reloadVestingSchedules', reloadVestingSchedules);

    return () => {
      socket.off('vestedTokenGrant', listenVestedTokenGrant);
      socket.off('reloadVestingSchedules', reloadVestingSchedules);
    }
  }, [ user ])

  useUpdateEffect(() => {
    setPaginatedVestingSchedule(applyPagination(
      filteredVestingSchedules,
      vestingSchedulePage,
      rowsPerPage
    ));
  }, [ filteredVestingSchedules, vestingSchedulePage, rowsPerPage ])

  const handlePageChange = async (newPage: number) => {
    try {
      setVestingSchedulePage(newPage);

      if(vestingSchedulePage < newPage) {
        const { status, data } = await VestingAPI.getVestingSchedules({
          limit: rowsPerPage * 2,
          skip: filteredVestingSchedules.length,
          address: connectedAddress,
          ...filters
        })

        if(status) {
          setVestingSchedulesCount(data.vestingSchedulesTotal);
          setFilteredVestingSchedule(prevState => prevState.concat(data.vestingSchedules));
        }
      }
    } catch(error) {
      console.log('error', error);
      toast.error('An unexpected error occurred. Please refresh your browser (F5).');
    }
  };

  const handleRowsPerPageChange = async (event: ChangeEvent<HTMLInputElement>) => {
    try {
      const rows = parseInt(event.target.value, 10);

      if(rows > filteredVestingSchedules.length) {
        const { status, data } = await VestingAPI.getVestingSchedules({
          limit: rows * 4,
          skip: filteredVestingSchedules.length,
          address: connectedAddress,
          ...filters
        })

        if(status) {
          setVestingSchedulesCount(data.vestingSchedulesTotal);
          setFilteredVestingSchedule(prevState => prevState.concat(data.vestingSchedules));
        }
      }

      setRowsPerPage(rows);

    } catch(error) {
      console.log('error', error);
      toast.error('An unexpected error occurred. Please refresh your browser (F5).');
    }
  };

  const TITLE = `${SITE_TITLE} | ${PAGE_TITLE}`;

  const {
    isAdmin,
    isTreasurer,
    tokensSource,
    allocationTypes,
    endDateTimestamp,
    claimedTokensByDate,
    claimableTokensByDate,
    vestingSchedulesTreasury
  } = user;

  const { claimable, claimed, locked, total, sources } = tokensSource;

  return (
    <>
      <Head> <title>{TITLE}</title> </Head>

      <Box component="main" sx={styles.containerMain} >
        <GridContainer mb={10}>
          <Grid item xs={12} md={6}>
            <Typography variant='h2'>
              TGE ML Tokens
            </Typography>
          </Grid>

          {isTreasurer && vestingSchedulesTreasury && !!vestingSchedulesTreasury.length && (
            <Grid item xs={12} md={6} display='grid' justifyContent='flex-end'>
              <Box>
                <NextLink href={ROUTES.createVestingSchedules} passHref>
                  <Button variant='contained'>
                    Create vesting schedules
                  </Button>
                </NextLink>
              </Box>
            </Grid>
          )}
        </GridContainer>

        <GridContainer mb={10}>
          <Grid item md={!isAdmin || (isAdmin && isTreasurer) ? 6 : 12} xs={12}>
            <MLTTokenOverview
              address={connectedAddress}
              tokensSource={tokensSource}
              allocationTypes={allocationTypes}
              endDateTimestamp={endDateTimestamp}
            />
          </Grid>

          {(!isAdmin || (isAdmin && isTreasurer)) && (
            sources.length ? (
              <Grid item md={6} xs={12}>
                <TokenDistribution
                  totalVesting={total}
                  lockedTokens={locked}
                  claimedTokens={claimed}
                  claimableTokens={claimable}
                />
              </Grid>
            ) : (
              <Grid item md={6} xs={12}>
                <Box display='flex' justifyContent='center'>
                  <Skeleton variant='circular' width={200} height={200} />
                </Box>
              </Grid>
            )
          )}

          {isAdmin && (
            <Grid item xs={12} mt={10}>
              <PoolTotalsTable/>
            </Grid>
          )}

          <Grid item xs={12} mt={10}>
            <VestingScheduleTable
              isAdmin={isAdmin}
              isTreasurer={isTreasurer}
              claiming={claiming}
              filters={filters}
              setFilters={setFilters}
              setClaiming={setClaiming}
              rowsPerPage={rowsPerPage}
              page={vestingSchedulePage}
              onPageChange={handlePageChange}
              vestingSchedules={paginatedVestingSchedule}
              vestingSchedulesCount={vestingSchedulesCount}
              onRowsPerPageChange={handleRowsPerPageChange}
            />
          </Grid>

          <Grid item xs={12} mt={10}>
            {claimedTokensByDate && claimableTokensByDate ? (
              <VestingSchedulesChart
                claimedTokensByDate={claimedTokensByDate}
                claimableTokensByDate={claimableTokensByDate}
              />
            ) : (
              <Skeleton animation='wave' variant='rectangular' height={500} />
            )}
          </Grid>
        </GridContainer>
      </Box>
    </>
  );
};

YourTokensPage.getLayout = (page) => (
  <AuthMinlayer>
    <ValidateNetwork>
      <MainLayout>
        {page}
      </MainLayout>
    </ValidateNetwork>
  </AuthMinlayer>
);

export default YourTokensPage;
