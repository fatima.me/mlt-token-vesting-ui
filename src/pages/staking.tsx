import Head from 'next/head';
import { Box, Grid, Typography, useMediaQuery } from '@mui/material';

import Reward from 'src/components/Reward';
import { SITE_TITLE } from 'src/constants';
import { globalStyles } from 'src/styles/global';
import { MLStats } from 'src/components/MLStats';
import MainLayout from "src/components/MainLayout";
import { MyStaking } from 'src/components/MyStaking';
import { GridContainer } from 'src/components/GridContainer';
import { ValidateNetwork } from "src/components/ValidateNetwork";
import { AuthMinlayer } from "src/components/authentication/AuthMinlayer";

/* types */
import type { NextPage } from "next";
import type { Theme } from '@mui/material';

const PAGE_TITLE = 'Staking';
const TITLE = `${SITE_TITLE} | ${PAGE_TITLE}`;

const Staking: NextPage = () => {
  const mdUp = useMediaQuery((theme: Theme) => theme.breakpoints.up('md'));

  return (
    <>
      <Head> <title>{TITLE}</title> </Head>
      <Box component="main" sx={globalStyles.containerMain}>
        <GridContainer mb={4}>
          <Grid item xs={12} md={6}>
            <Typography variant='h2'>
              Stake and earn rewards
            </Typography>
          </Grid>
        </GridContainer>

        <GridContainer my={6} spacing={0} columnSpacing={2}>
          <Grid item xs={12} md={6} mb={{xs: 2, md: 0}}>
            <MyStaking/>
          </Grid>

          <Grid item xs={12} md={6} display={'grid'} gap={2}>
            <Reward/>
            <Reward/>
          </Grid>
        </GridContainer>
      </Box>
    </>
  )
}


Staking.getLayout = (page) => (
  <AuthMinlayer>
    <ValidateNetwork>
      <MainLayout>
        {page}
      </MainLayout>
    </ValidateNetwork>
  </AuthMinlayer>
);

export default Staking;