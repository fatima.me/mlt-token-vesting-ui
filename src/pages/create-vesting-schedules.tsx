import dayjs from 'dayjs';
import Head from 'next/head';
import utc from 'dayjs/plugin/utc';
import { BigNumber } from 'ethers';
import { useEffect, useState, useMemo, useRef } from 'react';
import {
  Avatar,
  Box,
  Grid,
  Step,
  StepContent,
  StepLabel,
  Stepper,
  Typography
} from '@mui/material';

import { SITE_TITLE } from 'src/constants';
import { useWeb3 } from 'src/hooks/useWeb3';
import { CheckIcon } from 'src/icons/CheckIcon';
import { globalStyles } from 'src/styles/global';
import MainLayout from 'src/components/MainLayout';
import { ValidateNetwork } from 'src/components/ValidateNetwork';
import { VESTING_START_TIMESTAMP } from 'src/data/vesting_data_dapp';
import { AuthMinlayer } from 'src/components/authentication/AuthMinlayer';
import { TreasuryGuard } from 'src/components/authentication/TreasuryGuard';
import { AddBeneficiaries } from 'src/components/CreateVestingSchedules/AddBeneficiaries';
import {
  VerificationBeneficiaries
} from 'src/components/CreateVestingSchedules/VerificationBeneficiaries';
import {
  VestingSchedulesPreview
} from 'src/components/CreateVestingSchedules/VestingSchedulesPreview';

/* types */
import type { FC } from 'react';
import type { NextPage } from 'next';
import type { StepIconProps } from '@mui/material';
import type { Beneficiaries } from 'src/types/vesting';

const StepIcon: FC<StepIconProps> = (props) => {
  const { active, completed, icon } = props;

  const highlight = active || completed;

  return (
    <Avatar
      sx={{
        width: 40,
        height: 40,
        color: highlight && 'secondary.contrastText',
        backgroundColor: highlight && 'secondary.main',
      }}
      variant="rounded"
    >
      {completed ? <CheckIcon fontSize="small" /> : icon}
    </Avatar>
  );
};

const PAGE_TITLE = 'Create vesting schedules';

dayjs.extend(utc);

const vestingStartTimestamp = dayjs.utc(VESTING_START_TIMESTAMP * 1000);

const createVestingSchedulesPage: NextPage = () => {
  const { user, getFullInfoUser } = useWeb3();
  const [ activeStep, setActiveStep ] = useState(0);
  const _topRef = useRef<HTMLDivElement | null>(null);
  const [ beneficiaries, setBeneficiaries ] = useState<Beneficiaries[]>([])

  useEffect(() => {
    getFullInfoUser();
  }, [])

  const supply = useMemo(() => {
    let result = BigNumber.from(0);

    for(const { amount } of user.vestingSchedulesTreasury) {
      result = result.add(amount);
    }

    return result;
  }, [ JSON.stringify(user) ])

  const handleContinue = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
    _topRef.current?.scrollIntoView();
  };

  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
    _topRef.current?.scrollIntoView({ behavior: 'smooth' });
  };

  const { allocationTypes } = user;
  const allocationType = allocationTypes[0].type;

  const steps = [
    {
      label: 'Add beneficiaries',
      unmountOnExit: false,
      content: (
        <AddBeneficiaries
          supply={supply}
          onBack={handleBack}
          onContinue={handleContinue}
          beneficiaries={beneficiaries}
          vestingType={allocationType}
          setBeneficiaries={setBeneficiaries}
          vestingStartTimestamp={vestingStartTimestamp}
        />
      )
    },
    {
      label: 'Verification of beneficiaries',
      unmountOnExit: true,
      content: (
        <VerificationBeneficiaries
          supply={supply}
          onBack={handleBack}
          onContinue={handleContinue}
          beneficiaries={beneficiaries}
          setBeneficiaries={setBeneficiaries}
        />
      )
    },
    {
      label: 'Preview of the vesting schedules and submission of the transaction to the contract',
      unmountOnExit: true,
      content: (
        <VestingSchedulesPreview
          supply={supply}
          onBack={handleBack}
          onContinue={handleContinue}
          allocation={allocationType}
          beneficiaries={beneficiaries}
        />
      )
    },
  ];

  const TITLE = `${SITE_TITLE} | ${PAGE_TITLE}`;

  return (
    <>
      <Head> <title>{TITLE}</title> </Head>

      <Box component="main" sx={globalStyles.containerMain} >
        <Grid container >
          <Grid item xs={12} mb={2}>
            <Typography variant='h2' textAlign='center'>
              {PAGE_TITLE}
            </Typography>
          </Grid>

          <Grid item xs={12}>
            <Stepper
              ref={_topRef}
              orientation="vertical"
              activeStep={activeStep}
              sx={{
                '& .MuiStepConnector-line': {
                  ml: 1,
                  borderLeftWidth: 2,
                  borderLeftColor: 'divider',
                }
              }}
            >
              {steps.map((step, index) => {
                const { label, content, unmountOnExit = false } = step;

                return(
                  <Step key={label}>
                    <StepLabel StepIconComponent={StepIcon}>
                      <Typography sx={{ ml: 2 }} variant="overline" >
                        {label}
                      </Typography>
                    </StepLabel>

                    <StepContent
                      TransitionProps={{
                        unmountOnExit,
                        mountOnEnter: true,
                      }}
                      sx={{
                        ml: '20px',
                        borderLeftWidth: 2,
                        borderLeftColor: 'divider',
                        py: (activeStep === index) && 2,
                      }}
                    >
                      {content}
                    </StepContent>
                  </Step>
                )
              })}
            </Stepper>
          </Grid>
        </Grid>
      </Box>
    </>
  )
}

createVestingSchedulesPage.getLayout = (page) => (
  <AuthMinlayer>
    <ValidateNetwork>
      <TreasuryGuard>
        <MainLayout>
          {page}
        </MainLayout>
      </TreasuryGuard>
    </ValidateNetwork>
  </AuthMinlayer>
)


export default createVestingSchedulesPage;