import Head from 'next/head';
import Router from 'next/router';
import nProgress from 'nprogress';
import { Toaster } from 'react-hot-toast';
import { CacheProvider } from '@emotion/react';
import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider } from '@mui/material/styles';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';

/* types */
import { FC } from 'react';
import type { NextPage } from 'next';
import type { AppProps } from 'next/app';
import type { EmotionCache } from '@emotion/cache';

/* components */
import '../i18n';
import { createTheme } from '../theme';
import { RTL } from '../components/rtl';
import { SITE_TITLE } from 'src/constants';
import { Web3ContextProvider } from 'src/contexts/Web3Context';
import { createEmotionCache } from '../utils/create-emotion-cache';
import { SettingsConsumer, SettingsProvider } from '../contexts/settings-context';
import { AppContextProvider } from '../contexts/app.context';

type EnhancedAppProps = AppProps & {
  Component: NextPage;
  emotionCache: EmotionCache;
}

Router.events.on('routeChangeError', nProgress.done);
Router.events.on('routeChangeStart', nProgress.start);
Router.events.on('routeChangeComplete', nProgress.done);

const clientSideEmotionCache = createEmotionCache();

const App: FC<EnhancedAppProps> = (props) => {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;

  const getLayout = Component.getLayout ?? ((page) => page);

  const TITLE = `${SITE_TITLE} | Home`;

  return (
    <CacheProvider value={emotionCache}>
      <Head>
        <title>{TITLE}</title>
        <meta
          name="viewport"
          content="initial-scale=1, width=device-width"
        />
      </Head>
      <AppContextProvider>
        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <SettingsProvider>
            <SettingsConsumer>
              {({ settings }) => (
                <ThemeProvider
                  theme={createTheme({
                    direction: settings.direction,
                    responsiveFontSizes: settings.responsiveFontSizes,
                    mode: settings.theme
                  })}
                >
                  <Web3ContextProvider>
                    <RTL direction={settings.direction}>
                      <>
                        <CssBaseline />

                        <Toaster position="top-center" />

                        { getLayout(<Component {...pageProps} />) }
                      </>
                    </RTL>
                  </Web3ContextProvider>
                </ThemeProvider>
              )}
            </SettingsConsumer>
          </SettingsProvider>
        </LocalizationProvider>
      </AppContextProvider>
    </CacheProvider>
  );
};

export default App;
