import Head from 'next/head';
import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { useTranslation } from 'react-i18next';
import { Grid, Box, Typography, Link } from '@mui/material';

import { useWeb3 } from 'src/hooks/useWeb3';
import { WalletIcon } from 'src/icons/WalletIcon';
import MainLayout from 'src/components/MainLayout';
import { globalStyles as styles } from 'src/styles/global';
import { SplashScreen } from 'src/components/SplashScreen';
import { LAUNCH_URL, ROUTES, SITE_TITLE } from 'src/constants';
import Web3ModalButton from 'src/components/ConnectWallet/Web3ModalButton';

/* types */
import type { NextPage } from 'next';

const PAGE_TITLE = 'Connect wallet';

const MintlayerPage: NextPage = () => {
  const router = useRouter();
  const { t } = useTranslation();
  const { isInitialized, isAuthenticated, user, addresses } = useWeb3();

  const currentAddress = addresses[0] || '';

  useEffect(() => {
    if(isAuthenticated) {
      router.push(ROUTES.yourTokens);
    }
  }, [ isAuthenticated, user ])

  if(isAuthenticated || !isInitialized) {
    return <SplashScreen/>
  }

  const TITLE = `${SITE_TITLE} | ${PAGE_TITLE}`;

  return (
    <>
      <Head> <title>{TITLE}</title> </Head>

      <Box component="main" sx={styles.containerMain} >
        <Grid container spacing={2}>
          {(isInitialized && isAuthenticated || !currentAddress) ? (
            <Grid item xs={12} textAlign='center'>
              <Typography variant='h1'>
                Your Tokens
              </Typography>

              <Typography variant='subtitle1'>
                Connect your wallet, so you can see unlock dates for your tokens
              </Typography>

              <Box sx={styles.wrapperWalletIcon}>
                <WalletIcon sx={styles.walletIcon}/>
              </Box>

              <Web3ModalButton title={t('Connect Your Wallet')}/>
            </Grid>
          ) : (
            <Grid item xs={12}>
              <Box maxWidth={700} m='auto'>
                <Typography variant='h6' textAlign='center'>
                  {`Your Address is not whitelisted ${currentAddress}`}
                </Typography>

                <Box my={2}/>

                <Box textAlign='justify'>
                  <Typography variant='subtitle1'>
                    It seems that the address you are using is not allowed to this DApp. If you
                    have an account on our Launch site:
                  </Typography>

                  <Typography>
                    please check your token generation preferences in
                    <Link href={LAUNCH_URL} target='_blank'>{' this page'}</Link>.
                    If the address is the launch site is the same you are using now, please
                    send us an email
                    <Link href='mailto:support@mintlayer.org?Whitelist Issue' target='_blank'>
                    {' support@mintlayer.org'}
                    </Link>,
                    for further investigations. In the email please attach the screenshot of both pages. If you don't belong to launch site and are having issues, please send an email
                    <Link href='mailto:saft-support@mintlayer.org' target='_blank'>
                    {' saft-support@mintlayer.org'}
                    </Link>,
                    with a screenshot of this page.
                  </Typography>
                </Box>


                <Box sx={styles.wrapperWalletIcon}>
                  <WalletIcon sx={styles.walletIcon}/>
                </Box>

                <Box textAlign='center'>
                  <Web3ModalButton title={t('Connect Your Wallet')}/>
                </Box>
              </Box>
            </Grid>
          )}
        </Grid>
      </Box>
    </>
  );
};

MintlayerPage.getLayout = (page) => (
  <MainLayout>
    {page}
  </MainLayout>
);

export default MintlayerPage;
