import type { SxProps, Theme } from "@mui/system";

type S = SxProps<Theme>;

const containerMain: S = {
  pt: 12,
  pb: 10,
  flexGrow: 1,
  maxWidth: 'lg',
  margin: 'auto',
}

const wrapperWalletIcon: S = {
  p: 4,
  mb: 4,
  mt: 8,
  maxWidth: 200,
  marginX: 'auto',
  borderRadius: 100,
  background: '#DAEBE6',
};

const walletIcon: S = {
  width: '100%',
  height: '100%',
  'path': {
    fill: '#FFFFFF',
  }
};

export const globalStyles = {
  walletIcon,
  containerMain,
  wrapperWalletIcon,
}