/* types */
import type { Dayjs } from "dayjs";
import type { BigNumber, BigNumberish } from "ethers";
import type { User, VestingScheduleWithProof } from "./web3";
import type { ChangeEvent, Dispatch, SetStateAction } from "react";
import type { VestingSchedule } from "@mintlayer/vesting-tree/dist/types";

export interface MLTTokenOverviewProps extends Pick<
  User,
  | 'tokensSource'
  | 'allocationTypes'
> {
  address: string;
  endDateTimestamp: number;
};

export interface TokenDistributionProps {
  totalVesting: number;
  lockedTokens: number;
  claimedTokens: number;
  claimableTokens: number;
}

export interface VestingScheduleSchartProps extends Pick<
  User,
  | 'claimedTokensByDate'
  | 'claimableTokensByDate'
>{}

export interface VestingScheduleTableProps {
  claiming: boolean;
  page: number;
  rowsPerPage: number;
  isAdmin: boolean;
  isTreasurer: boolean;
  vestingSchedulesCount: number;
  filters: VestingScheduleAdminFilters;
  setFilters: Dispatch<SetStateAction<VestingScheduleAdminFilters>>;
  setClaiming: Dispatch<SetStateAction<boolean>>;
  vestingSchedules: VestingScheduleWithProof[];
  onPageChange?: (newPage: number) => void;
  onRowsPerPageChange?: (event: ChangeEvent<HTMLInputElement>) => void;
}

export interface VestingScheduleAdminFilters {
  beneficiary: string;
  unclaimed?: boolean;
  unlocked?: boolean;
}

export interface getVestingSchedulesProps {
  skip: number;
  limit: number;
  address: string;
}

export interface StepProps {
  onBack?: () => void;
  onContinue?: () => void;
}

export type TemplateType =
  | 'blank'
  | 'default';

export interface AddBeneficiariesProps extends StepProps {
  supply: BigNumber;
  vestingType: string;
  vestingStartTimestamp: Dayjs;
  beneficiaries: Beneficiaries[];
  setBeneficiaries: Dispatch<SetStateAction<Beneficiaries[]>>;
}

export interface VerificationBeneficiariesProps extends StepProps {
  supply: BigNumber;
  beneficiaries: Beneficiaries[];
  setBeneficiaries: Dispatch<SetStateAction<Beneficiaries[]>>;
}

export interface VestingSchedulesPreviewProps extends StepProps {
  supply: BigNumber;
  allocation: string;
  beneficiaries: Beneficiaries[];
}

export interface Beneficiaries {
  address: string;
  amount: string;
}

export interface BeneficiariesTableProps {
  beneficiaries: Beneficiaries[];
  setBeneficiaries: Dispatch<SetStateAction<Beneficiaries[]>>;
}

export interface ProcessBeneficiariesResponse {
  status: boolean;
  message?: string;
  data: VestingTreeData;
}

export interface VestingTreeData {
  root: string;
  uriIPFS: string;
  proofBalance: string[];
  vestingTypeProof: string[];
  allocationQuantityProof: string[];
  vestingSchedules: VestingSchedule[];
}

export interface TreeLog {
  treeLogTx: string;
  treeLogId: number;
  status: boolean;
  userAddress: string;
}
export interface ReloadVestingSchedules {
  vestingSchedules: VestingScheduleWithProof[];
  vestingSchedulesTotal: number;
  userAddress: string;
}

export interface AllocationWithTokenCount {
  [type: string]: {
		label: string;
		poolShare: number;
		tokenAmount: string;
		vestingType: string;
    lockedTokens: BigNumberish;
    claimedTokens: BigNumberish;
	  claimableTokens: BigNumberish;
	}
}