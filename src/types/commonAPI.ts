import type { NextApiResponse } from 'next/types';

export interface IResponseAPI<T> {
  data: T;
  status: boolean;
  message: string;
}

export interface Response<T = any> extends NextApiResponse<IResponseAPI<T>>{}

export interface Cache {
  lastScannedBlockNumber: number;
  events: any[];
}
