import type { ReactNode } from 'react';

export interface Settings {
  direction?: 'ltr' | 'rtl';
  responsiveFontSizes?: boolean;
  theme: 'light' | 'dark';
}

export interface SettingsContextValue {
  settings: Settings;
  saveSettings: (update: Settings) => void;
}

export interface SettingsProviderProps {
  children?: ReactNode;
}