export interface Web3ModalProps {
  open: boolean;
  onClose: () => void;
}

export interface Web3ModalButtonProps {
  title?: string;
}