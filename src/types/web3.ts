/* types */
import type { ReactNode } from "react";
import type { providers } from "ethers";
import type { VestingType } from "@mintlayer/vesting-tree/dist/types";

export interface ConnectWalletResult {
  data: null | {
    addresses: string[];
  };
  message: any;
  status: boolean;
  hasProvider: boolean;
}

export interface VestingSchedule {
  userAddress: string; // Address of beneficiary
  amount: string; // Amount of tokens to be released
  vestingCliff: number; // Lock delay for release
}

export interface VestingScheduleWithProof extends VestingSchedule {
  hash: string;
  proof: string[];
  transactionHash: string;
  vestingClaimed: boolean;
  tree: {
    root: string
  }
}

export interface TokenSource {
  total: number;
  allocationType: string;
}

export interface TokensSource {
  sources: TokenSource[];
  total: number;
  locked: number;
  claimed: number;
  claimable: number;
}

export interface User {
  address: string;
  isAdmin: boolean;
  isTreasurer: boolean;
  endDateTimestamp: number;
  vestingType?: VestingType;
  tokensSource?: TokensSource;
  vestingTypeProof?: string[];
  allocationTypes?: { type: string }[];
  claimedTokensByDate?: { [cliff: string]: number };
  claimableTokensByDate?: { [cliff: string]: number };
  vestingSchedulesTreasury?: VestingScheduleWithProof[];
  trees: {
    id: string;
    root: string;
    treasuryAllocationProof: {
      allocationTypeProof: string[];
    }[];
  }[];
}

export interface Web3State {
  user: User | null;
  addresses: string[];
  isInitialized: boolean;
  isAuthenticated: boolean;
  getFullInfoUser: () => Promise<void>;
  provider: providers.Web3Provider | null;
  registerUser: (address: string, hasProvider: boolean) => Promise<void>;
}

export interface Web3ProviderProps {
  children: ReactNode;
}

export enum Web3TypeAction {
  SET_ADDRESSES = 'SET_ADDRESSES',
  SET_PROVIDER = 'SET_PROVIDER',
  SET_INITIALIZED = 'SET_INITIALIZED',
  REGISTER = 'REGISTER',
  DISCONNECT = 'DISCONNECT',
}

export type Web3TypesActions = keyof typeof Web3TypeAction;


export type Web3Handlers = Record<
  Web3TypesActions,
  (state: Web3State, action: Web3StateAction) => Web3State
>

export type SetAddressesAction = {
  type: Web3TypeAction.SET_ADDRESSES;
  payload: { addresses: string[] };
}

export type SetRegisterAction = {
  type: Web3TypeAction.REGISTER;
  payload: {
    user: User;
  };
}

export type SetDisconnectAction = {
  type: Web3TypeAction.DISCONNECT;
}

export type SetInitializedAction = {
  type: Web3TypeAction.SET_INITIALIZED;
}

export type SetProviderAction = {
  type: Web3TypeAction.SET_PROVIDER;
  payload: { provider: providers.Web3Provider };
}

export type Web3StateAction =
  | SetAddressesAction
  | SetProviderAction
  | SetInitializedAction
  | SetRegisterAction
  | SetDisconnectAction


export interface Web3ContextValues extends Web3State {
  connectWallet: () => Promise<void>
}