export {};

export type PartialRecord<K extends keyof any, T> = {
  [P in K]?: T;
};

declare global {
  interface Window {
    ethereum?: any;
    sa_loaded?: boolean;
    sa_event?: (eventName: string) => void;
  }
}