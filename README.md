# ML Token vesting UI

The following repository contains the Dapp user interface through which users will be able to connect and claim their allocations

## enviroment
- node >= 16.13.0

## Development

Install dependencies

```bash
$ yarn install
```

copy the file .env.example to .env in it you will have the necessary data to run a development environment

```bash
$ cp .env.example .env
```

**Run**

```bash
$ yarn dev
```

## debug

```shell

export DEBUG="pw:api";
export PWDEBUG=1  # or export PWDEBUG=console

yarn dev
```

yarn e2e:dev tests/journeys/Vesting.spec.ts 
