import { test, expect } from "@playwright/test";

// Run tests in this file with portrait-like viewport.
// test.use({ viewport: { width: 600, height: 900 } });

test.describe("check no wallet", () => {

  test("wallet injected", async ({ page, baseURL }) => {
    
    await page.goto(baseURL, { waitUntil: 'load' });
    const href = await page.evaluate(() => document.location.href);
    await expect(href).toMatch(`${baseURL}`); // to equal
    await page.screenshot({ path: "screenshot/nowallet.png", fullPage: true  });
    
    // first page
    await expect(page.locator('header:has-text("Connect wallet")').getByRole('link')).toBeVisible();
    await expect(page.getByRole('button', { name: 'Connect wallet' })).toBeVisible()
    await expect(page.getByRole('heading', { name: 'Your Tokens' })).toBeVisible()
    await expect(page.getByRole('heading', { name: 'Connect your wallet to know when your tokens will be unlocked' })).toBeVisible()
    await expect(page.locator('main:has-text("Your TokensConnect your wallet to know when your tokens will be unlockedConnect ") div').nth(2)).toBeVisible()
    await expect(page.getByRole('button', { name: 'Connect Your Wallet' })).toBeVisible()

    const ethereum = await page.evaluate(() => window.ethereum );
    await expect(ethereum).toBeUndefined()

    await page.pause();
  });


});
