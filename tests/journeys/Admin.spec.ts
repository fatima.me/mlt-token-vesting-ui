import { test, expect } from "@playwright/test";
import { addWalletScript, injectMockWallet, sniffRequests } from "./helpers";

import network from '../../tests/fixtures/network.json'
import contract from '../../tests/fixtures/contract.json'
import pools from '../../tests/fixtures/pools.json'
import account from '../../tests/fixtures/admin.json'

const [
  preSeed,
  seed,
  marketing,
  longvesting,
  shortvesting,
  development,
  community,
  companyreserve,
  teamandadvisors
] = pools


const vestingTotal = '400,000,000.00'
const vestingType = 'N/A'

test.describe("admin", () => {
  
    test(`admin dashboard`, async ({ page, baseURL }) => {
      const { address, privateKey } = account  
      const { rpcUrl, chainId } = network
      // config
      // Subscribe to 'request' and 'response' events.
      sniffRequests(page, account.name)
      
      // start
      await addWalletScript(page)
      await page.goto(baseURL, { waitUntil: 'load' });
  
      const ethereum0 = await page.evaluate(() => window.ethereum );
      await expect(ethereum0).toBeUndefined()
      // inject mock wallet
      await injectMockWallet(page, {url: rpcUrl,address, privateKey, networkVersion: chainId, debug: true})
      // await page.waitForTimeout(3000)
      const ethereum = await page.evaluate(() => window.ethereum );
      await expect(ethereum).toBeTruthy()
  
      // connect with wallet 
      await page.getByRole('button', { name: 'Connect wallet' }).click();

      await Promise.all([
        page.waitForResponse(resp => resp.url().includes('/admin-dashboard')), // && resp.status() === 400
        page.waitForResponse(resp => resp.url().includes('/api/vesting-schedules')),
        page.waitForResponse(resp => resp.url().includes('/api/user')),
        page.getByRole('button', { name: 'MetaMask' }).click() // private option
      ]);
 
      // your-tokens page      
      await expect(await page.evaluate(() => document.location.href)).toMatch(`${baseURL}/admin-dashboard`);
      // header
      await expect(page.locator("text=TGE MLT Tokens")).toBeVisible({timeout: 50000})    
  
      // overview 
      // text   
      await expect(page.getByRole('heading', { name: 'MLT Token address' })).toBeVisible();
      await expect(page.getByRole('link', { name: contract.address })).toBeVisible()
      await expect(page.getByRole('heading', { name: 'Beneficiary' })).toBeVisible();
      await expect(await page.locator(`text=${address}`)).toBeVisible()      
      await expect(page.getByRole('heading', { name: 'Start date' })).toBeVisible();
      // date start value
      await expect(page.getByRole('heading', { name: 'End date' })).toBeVisible();
      // date end value
      
      await expect(page.getByRole('heading', { name: 'Vesting type' })).toBeVisible();
      await expect(page.getByText(vestingType)).toBeVisible()
    
      await expect(page.getByRole('heading', { name: 'Total vesting' })).toBeVisible();
      await page.getByRole('row', { name: `Total vesting ${vestingTotal}` }).getByText(vestingTotal).click();
  
      await expect(page.getByRole('heading', { name: 'Claimed tokens' })).toBeVisible();
      // Claimed tokens values
      await expect(page.getByRole('heading', { name: 'Claimable tokens' })).toBeVisible();
      // Claimable tokens values
      await expect(page.getByRole('heading', { name: 'Locked tokens' })).toBeVisible();    
      // locked tokens values
  
  
      // chart 
      await expect(await page.getByRole('heading', { name: 'Token Distribution' })).toBeVisible();
      // an chart
  
 
      // filter
      // unclaimed unlocked
      // search
      await page.getByPlaceholder('Search beneficiary').fill(seed.account.address);
      // vesting table
      // header
      await expect(await page.getByRole('heading', { name: 'Vesting Schedule' })).toBeVisible();
      await expect(await page.getByRole('columnheader', { name: 'Date' })).toBeVisible();
      await expect(await page.getByRole('columnheader', { name: 'Beneficiary' })).toBeVisible();
      await expect(await page.getByRole('columnheader', { name: 'Claimed tokens' })).toBeVisible();
      await expect(await page.getByRole('columnheader', { name: 'Claimable tokens' })).toBeVisible();
      await expect(await page.getByRole('columnheader', { name: 'Locked tokens' })).toBeVisible();
      // cell actions
      // body      
      await page.waitForTimeout(30000)// wait load/render all vesting-schedules
      await page.screenshot({ path: `screenshot/admin-wait.png`, fullPage: true  });

      const table = await page.locator('xpath=//*[text()="Vesting Schedule"]/../.. >> role=table')
      const rows = await table.getByRole('row')
      const count = await rows.count();
      await expect(count).toBeGreaterThan(1)
      for (let i = 1; i < count; i++) {
        const [, release, beneficiary, claimed, claimable, locked, action] = (await rows.nth(i).allInnerTexts())[0].split('\t')
        expect(beneficiary).toMatch('0x')
        const beneficiaryLink = await rows.nth(i).getByRole('link', { name: beneficiary })
        const link = await beneficiaryLink.getAttribute('href')
        expect(link).toMatch(seed.account.address)
        
        if( action === 'View in explorer' ) {
          expect(claimed).not.toEqual('0.00')
          expect(claimable).not.toEqual('0.00')
          expect(locked).toEqual('0.00')
        } else if( action === 'Release' ) {
          expect(claimed).toEqual('0.00')
          const button = await rows.nth(i).getByRole('button', { name: 'Release' })
          if(claimable === '0.00') {
            expect(locked).not.toEqual('0.00')
            expect(button).toBeDisabled()
          } else {
            expect(locked).toEqual('0.00')
            expect(button).toBeEnabled()
          }
        }
      }
      // pagination
  
      await page.waitForTimeout(1500)
    });
  

});
