import { test, expect, type Page } from "@playwright/test";
import { addWalletScript, injectMockWallet } from "./helpers";

import network from "../../tests/fixtures/network.json";
import contract from "../../tests/fixtures/contract.json";
import pools from "../../tests/fixtures/pools.json";

const [
  preSeed,
  seed,
  marketing,
  longvesting,
  shortvesting,
  development,
  community,
  companyreserve,
  teamandadvisors,
] = pools;

const { account, pool } = seed;

test.describe("check static text", () => {
  test("check footer wallet injected seed", async ({ page, baseURL }) => {
    const { total, type } = pool;
    const { address, privateKey } = account;
    const { rpcUrl, chainId } = network;

    // config
    // Subscribe to 'request' and 'response' events.
    // sniffRequests(page, account.name)

    // start
    await addWalletScript(page);
    await page.goto(baseURL, { waitUntil: "load" });
    const href = await page.evaluate(() => document.location.href);
    await expect(href).toMatch(`${baseURL}`); // to equal
    await page.screenshot({ path: "screenshot/footer.png", fullPage: true });

    // inject mock wallet
    await injectMockWallet(page, {
      url: rpcUrl,
      address,
      privateKey,
      networkVersion: chainId,
      debug: true,
    });
    await page.waitForTimeout(3000);
    const ethereum = await page.evaluate(() => window.ethereum);
    await expect(ethereum).toBeTruthy();

    // connect with wallet
    await page.getByRole("button", { name: "Connect wallet" }).click();
    await Promise.all([
      page.waitForResponse((resp) => resp.url().includes("/your-tokens")), // && resp.status() === 400
      page.waitForResponse((resp) =>
        resp.url().includes("/api/vesting-schedules")
      ),
      page.waitForResponse((resp) => resp.url().includes("/api/user")),
      page.getByRole("button", { name: "MetaMask" }).click(), // private option
    ]);

    //Footer//
    await page.locator("div > div > div > .css-0").first().click();
    const [page1] = await Promise.all([
      page.waitForEvent("popup"),
      page.locator(".MuiBox-root > a").first().click(),
    ]);
    await page1.close();
    const [page2] = await Promise.all([
      page.waitForEvent("popup"),
      page.locator("a:nth-child(2)").click(),
    ]);
    await page2.close();
    const [page3] = await Promise.all([
      page.waitForEvent("popup"),
      page.locator("a:nth-child(3)").click(),
    ]);
    await page3.close();
    const [page4] = await Promise.all([
      page.waitForEvent("popup"),
      page.locator("a:nth-child(4)").click(),
    ]);
    await page4.close();
    const [page5] = await Promise.all([
      page.waitForEvent("popup"),
      page.locator("a:nth-child(5)").click(),
    ]);
    await page5.close();
    await page
      .getByText("2022 Mintlayer © is designed by RBB S.r.l. - C.O.E. SM 28251")
      .click();
    await page
      .getByText(
        "Certified as High Technology Entrerprise by San Marino innovation."
      )
      .click();
    await page.getByText("All Rights Reserved.").click();
  });
});
