import { test, expect } from "@playwright/test";
import { addWalletScript, injectMockWallet, sniffRequests } from "./helpers";

import network from '../../tests/fixtures/network.json'
import contract from '../../tests/fixtures/contract.json'
import pools from '../../tests/fixtures/pools.json'


const [
  preSeed,
  seed,
  marketing,
  longvesting,
  shortvesting,
  development,
  community,
  companyreserve,
  teamandadvisors
] = pools

const {account, pool } = seed

test.describe("pool types", () => {
    test.beforeEach(async ({ page, baseURL })  => {
      const { address, privateKey } = account  
      const { rpcUrl, chainId } = network

      // check if have funds

      // config
      // Subscribe to 'request' and 'response' events.
      sniffRequests(page, account.name)
      
      // start
      await addWalletScript(page)    
      await page.goto(baseURL, { waitUntil: 'load' });
  
      const ethereum0 = await page.evaluate(() => window.ethereum );
      await expect(ethereum0).toBeUndefined()
      // inject mock wallet
      await injectMockWallet(page, {url: rpcUrl,address, privateKey, networkVersion: chainId, debug: true})
      // await page.waitForTimeout(3000)
      const ethereum = await page.evaluate(() => window.ethereum );
      await expect(ethereum).toBeTruthy()
  
      // connect with wallet 
      await page.getByRole('button', { name: 'Connect wallet' }).click();

      await Promise.all([
        page.waitForResponse(resp => resp.url().includes('/your-tokens')), // && resp.status() === 400
        page.waitForResponse(resp => resp.url().includes('/api/vesting-schedules')),
        page.waitForResponse(resp => resp.url().includes('/api/user')),
        page.getByRole('button', { name: 'MetaMask' }).click() // private option
      ]);    

    })
  
    test.skip(`claim tokens`, async ({ page, baseURL }) => { 
      // time to render
      await page.waitForTimeout(3000)
      await page.screenshot({ path: `screenshot/claim-${pool.type}.png`, fullPage: true  });

      // your-tokens page      
      await expect(await page.evaluate(() => document.location.href)).toMatch(`${baseURL}/your-tokens`);
  
      const parentL = page.locator('//button[text()="Claim"]/../..')
      const claimables = await parentL.count()
      if (claimables > 0 ) {
        const parent = await parentL.first()
        const dateClaim = (await parent.getByRole('cell').nth(1).allInnerTexts()).join()
        const parentAgain = await page.locator(`//*[text()="${dateClaim}"]/..`)
        
        const button = await parent.first().getByRole('button', { name: 'Claim' })
        await expect(button).toBeVisible() 
        let jsonTx
        await Promise.all([
          page.waitForResponse(async resp => { 
            resp.url().includes('/rpc')
            jsonTx = await resp.json()
            return true
          }), // && resp.status() === 400
          button.click()
        ])
        await page.waitForTimeout(15000)

        const explorer = await parentAgain.getByRole('link', { name: 'View in explorer' })
        await expect(explorer).toBeVisible()
        const [page1] = await Promise.all([
          page.waitForEvent('popup'),
          explorer.click()
        ]);
        // await expect (page1.getByText('0x8fc99e68c13f8e7a507b5c8e8fba66caeb290e8c546194324064aa18403e436f')).toBeVisible();
        await page1.close();
      }
      
      await page.waitForTimeout(1000)
    });
  

});
