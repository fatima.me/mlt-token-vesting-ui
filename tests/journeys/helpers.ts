import { Page } from "@playwright/test";
import { existsSync, mkdirSync, readFileSync, writeFileSync } from "fs";

export async function addInitScript(page: Page, path, fn: Function)  {
  await page.addInitScript({
    content: readFileSync(path, "utf-8") + `\n(${fn.toString()})();`,
  });
};

export async function addWalletScript(page){
  await addInitScript(page, 'node_modules/ethers/dist/ethers.umd.js', () => {
    console.log('window._ethers', window._ethers )
  })    
  await addInitScript(page, 'tests/dist/MockWallet.js', () => {
    console.log('injected MockWallet')
  })
}


export async function injectMockWallet(page, setup) {
  await page.evaluate((setup) => {
    window.ethereum = new MockProvider(
      setup
    ); // from globalThis and project app
  }, setup);
}


const regexp = /[^/\\&\?]+\.\w{3,4}(?=([\?&].*$|$))/i

export function sniffRequests(page: Page, type){
  //const dest = `tests/fixtures/responses/${type}/`
  //!existsSync(dest) && mkdirSync(dest);
  //page.on('response', async response => {
  //  const contentType = await response.headerValue("content-type");    
  //  if (contentType && contentType.indexOf("application/json") !== -1 && response.status()===200) {
  //    // const data = await response.json()
  //    // writeFileSync(`${dest}/${response.url().replace(/[^a-z0-9]/gi, '_').toLowerCase()}`, JSON.stringify(data, null, 2)  )
  //  } else {
  //    console.log('>>', response.status(), response.url());
  //  }
  //
  //})
}