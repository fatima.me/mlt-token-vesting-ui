import { test, expect } from "@playwright/test";
import { addWalletScript, injectMockWallet, sniffRequests } from "./helpers";

import network from '../../tests/fixtures/network.json'
import contract from '../../tests/fixtures/contract.json'
import pools from '../../tests/fixtures/pools.json'

test.describe("pool types", () => {
  
  pools.filter(p => !p.skip).forEach(({account, pool}) => {
  
    test(`wallet injected with account ${pool.type}`, async ({ page, baseURL }) => {  
      const { total, type } =  pool   
      const { address, privateKey } = account  
      const { rpcUrl, chainId } = network

      // config
      // Subscribe to 'request' and 'response' events
      sniffRequests(page, pool.type)
      
      // start
      await addWalletScript(page)    
      await page.goto(baseURL, { waitUntil: 'load' });
  
      const ethereum0 = await page.evaluate(() => window.ethereum );
      await expect(ethereum0).toBeUndefined()
      // inject mock wallet
      await injectMockWallet(page, {url: rpcUrl,address, privateKey, networkVersion: chainId, debug: true})
      // await page.waitForTimeout(3000)
      const ethereum = await page.evaluate(() => window.ethereum );
      await expect(ethereum).toBeTruthy()
  
      // connect with wallet 
      await page.getByRole('button', { name: 'Connect wallet' }).click();

      await Promise.all([
        page.waitForResponse(resp => resp.url().includes('/your-tokens')), // && resp.status() === 400
        page.waitForResponse(resp => resp.url().includes('/api/vesting-schedules')),
        page.waitForResponse(resp => resp.url().includes('/api/user')),
        page.getByRole('button', { name: 'MetaMask' }).click() // private option
      ]);

      // time to render
      await page.waitForTimeout(3000)
      await page.screenshot({ path: `screenshot/vesting-${type}.png`, fullPage: true  });

      // your-tokens page
      await expect(await page.evaluate(() => document.location.href)).toMatch(`${baseURL}/your-tokens`); // to equal       
      // header
      await expect(page.locator("text=Your MLT Tokens")).toBeVisible({timeout: 50000})    
      // button
      await page.getByRole('button', { name: 'Get Your Tokens' }).click();
  
      // overview 
      await expect(page.getByRole('heading', { name: 'MLT Token address' })).toBeVisible();
      await expect(page.getByRole('link', { name: contract.address })).toBeVisible()      
      await expect(page.getByRole('heading', { name: 'Beneficiary' })).toBeVisible();
      await expect(page.locator(`text=${address}`)).toBeVisible()
      await expect(page.getByRole('heading', { name: 'Start date' })).toBeVisible();
      // a date start
      await expect(page.getByRole('heading', { name: 'End date' })).toBeVisible();
      // a date end      
      await expect(page.getByRole('heading', { name: 'Vesting type' })).toBeVisible();
      await expect(page.getByRole('link', { name: type })).toBeVisible()
      await expect(page.getByRole('heading', { name: 'Total vesting' })).toBeVisible();
      await page.getByRole('row', { name: `Total vesting ${total}` }).getByText(total).click();  
      await expect(page.getByRole('heading', { name: 'Claimed tokens' })).toBeVisible();
      // a digit
      await expect(page.getByRole('heading', { name: 'Claimable tokens' })).toBeVisible();
      // a digit
      await expect(page.getByRole('heading', { name: 'Locked tokens' })).toBeVisible();    
      // a digit  
  
      // chart 
      await expect(await page.getByRole('heading', { name: 'Token Distribution' })).toBeVisible();
      // a chart
  
      // vesting table
      // header
      await expect(await page.getByRole('heading', { name: 'Vesting Schedule' })).toBeVisible();
      await expect(await page.getByRole('columnheader', { name: 'Date' })).toBeVisible();
      await expect(await page.getByRole('columnheader', { name: 'Claimed tokens' })).toBeVisible();
      await expect(await page.getByRole('columnheader', { name: 'Claimable tokens' })).toBeVisible();
      await expect(await page.getByRole('columnheader', { name: 'Locked tokens' })).toBeVisible();
      // body
      const table = await page.locator('xpath=//*[text()="Vesting Schedule"]/../.. >> role=table')
      const rows = await table.getByRole('row')
      const count = await rows.count();
      for (let i = 1; i < count; i++) {
        const [, release, claimed, claimable, locked, action] = (await rows.nth(i).allInnerTexts())[0].split('\t')
        if( action === 'View in explorer' ) {
          expect(claimed).not.toEqual('0.00')
          expect(claimable).not.toEqual('0.00')
          expect(locked).toEqual('0.00')
        } else if( action === 'Claim' ) {
          expect(claimed).toEqual('0.00')
          const button = await rows.nth(i).getByRole('button', { name: 'Claim' })
          if(claimable === '0.00') {
            expect(locked).not.toEqual('0.00')
            expect(button).toBeDisabled()
          } else {
            expect(locked).toEqual('0.00')
            expect(button).toBeEnabled()
          }
       }
      }

      await page.waitForTimeout(1500)

    });
  })
  


});
