// depend on ethers module is on window

type ProviderSetup = {
  url: string
  address: string
  privateKey: string
  networkVersion: number
  debug?: boolean
  manualConfirmEnable?: boolean
}

interface IMockProvider {
  request(args: { method: 'eth_accounts'; params: string[] }): Promise<string[]>
  request(args: { method: 'eth_requestAccounts'; params: string[] }): Promise<string[]>

  request(args: { method: 'net_version' }): Promise<number>
  request(args: { method: 'eth_chainId'; params: string[] }): Promise<string>

  request(args: { method: 'personal_sign'; params: string[] }): Promise<string>
  request(args: { method: 'eth_decrypt'; params: string[] }): Promise<string>

  request(args: { method: string, params?: any[] }): Promise<any>
}

// eslint-disable-next-line import/prefer-default-export
export class MockProvider implements IMockProvider {
  private setup: ProviderSetup
  private _provider: Object

  public isMetaMask = true

  private acceptEnable?: (value: unknown) => void

  private rejectEnable?: (value: unknown) => void

  constructor( setup: ProviderSetup) {
    this._provider = new _ethers.providers.JsonRpcProvider(setup.url)
    this.setup = setup
  }

  // eslint-disable-next-line no-console
  private log = (...args: (any | null)[]) => this.setup.debug && console.log('🦄', ...args)

  get selectedAddress(): string {
    return this.setup.address
  }

  get networkVersion(): number {
    return this.setup.networkVersion
  }

  get chainId(): string {
    return `0x${this.setup.networkVersion.toString(16)}`
  }

  answerEnable(acceptance: boolean) {
    if (acceptance) this.acceptEnable!('Accepted')
    else this.rejectEnable!('User rejected')
  }

  request({ method, params }: any): Promise<any> {
    this.log(`request[${method}]`)

    switch (method) {
      case 'eth_requestAccounts':
      case 'eth_accounts':
        if (this.setup.manualConfirmEnable) {
          return new Promise((resolve, reject) => {
            this.acceptEnable = resolve
            this.rejectEnable = reject
          }).then(() => [this.selectedAddress])
        }
        return Promise.resolve([this.selectedAddress])

      case 'net_version':
        return Promise.resolve(this.setup.networkVersion)

      case 'eth_chainId':
        return Promise.resolve(this.chainId)

      case 'eth_sendTransaction': 
      const gasPrice = this._provider.getGasPrice() // promise

      // new _ethers.Wallet.fromMnemonics(...words);      
      const wallet = new _ethers.Wallet(this.setup.privateKey);
      const signer = wallet.connect(this._provider)

      //tx = {from, to, value, gasPrice, gasLimit, nonce}
      const gasLimit = _ethers.utils.hexlify(100000)
      const nonce = this._provider.getTransactionCount(wallet.address, 'latest')
      const tx = {... params[0], gasPrice, gasLimit } // nonce
    
      const transaction = signer.sendTransaction(tx)

      return transaction

      case 'eth_getLogs':
      case 'eth_blockNumber': {
        return this._provider.send(method, params )
        // The method eth_blockNumber is not implemented by the mock provider.
        // this.log(`resquesting missing implement method ${method}`)
        // return Promise.reject(`The method ${method} is bad implemented by the mock provider.`)
        // await this.provider.send("eth_blockNumber");
      }


      default:
        this.log(`resquesting missing method ${method}`)
        // eslint-disable-next-line prefer-promise-reject-errors
        return Promise.reject(`The method ${method} is not implemented by the mock provider.`)
        // The method wallet_switchEthereumChain is not implemented by the mock provider.        
    }
  }
  decrypt(arg0: { encryptedData: any; privateKey: string }): string {
    return this._decrypt(arg0)
    // throw new Error("Method not implemented.")
  }
  personalSign(arg0: { privateKey: Buffer; data: any }): string {
    return this.personalSign(arg0)
    // throw new Error("Method not implemented.")
  }

  sendAsync(props: { method: string }, cb: any) {
    switch (props.method) {
      case 'eth_accounts':
        cb(null, { result: [this.setup.address] })
        break;

      case 'net_version': cb(null, { result: this.setup.networkVersion })
        break;

      default: this.log(`Method '${props.method}' is not supported yet.`)
    }
  }

  on(props: string) {
    this.log('registering event:', props)
  }

  removeListener() {
    this.log('removeListener', null)
  }


  removeAllListeners() {
    this.log('removeAllListeners', null)
  }
}

// moved to src/contexts/Web3Context.tsx  to export 
// globalThis.ethers = ethers;
globalThis.MockProvider = MockProvider
