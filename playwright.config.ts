import type { PlaywrightTestConfig } from '@playwright/test';
import { devices } from '@playwright/test';

const PORT = process.env.PORT || 3000
console.log('PORT', PORT);


const config: PlaywrightTestConfig = {
  testDir: './tests',
  // outputDir: 'test-results/',
  timeout: 40 * 1000,
  expect: {
    timeout: 5000
  },
  fullyParallel: true,
  forbidOnly: !!process.env.CI,
  retries: process.env.CI ? 2 : 0,
  workers: process.env.CI ? 1 : undefined,
  reporter: 'html',
  use: {
    actionTimeout: 0,
    baseURL: `http://localhost:${PORT}`,    
    trace: 'on-first-retry', // on
  },
  projects: [
    {
      name: 'chromium',
      use: {
        ...devices['Desktop Chrome'],
      },
    },
  ],
  webServer: {
    command: `yarn build && yarn e2e:build && PORT=${PORT} yarn start`,
    // port: Number(PORT),
    url: `http://localhost:${PORT}`,
    timeout: 120 * 1000,
    reuseExistingServer: !process.env.CI,
  },
};
// console.log('config', config)

export default config;
