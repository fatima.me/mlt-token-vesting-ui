const APP_NAME = process.env.APP_NAME;

module.exports = {
  apps: [
    {
      script: "yarn start",
      name: APP_NAME,
      watch: false,
      max_restarts: 10,
      autorestart: true,
      restart_delay: 1000, // 1 seg
      env: {
        NODE_ENV: 'production'
      }
    },
  ],
};